#Adminsite

Para levantar en modo dev, levantar el server desde el directorio padre:

```
cd ..
python -m SimpleHTTPServer 8000
```
y navegar a http://localhost:8000/adminsite


-Para "Iniciar sesión" colocamos el usuario "asdra_admin" y la contraseña "Admin.Asdra2016" utilizando OAuth.

-Una vez dentro tenemos una primer columna con simbolos "+" para expandir y ver los comentarios si es que los hay.

-Para editar los campos hacemos doble click, editamos y luego presionamos el boton "Guardar", que esta debajo de la tabla de sitios, el guardado es automatico sin aviso (Se puede mejorar con una alerta o popup). Lo mismo para editar los comentarios.

-Ademas de editar podemos "moderar" los sitios y sus comentarios editando la columna "Validado" a true en caso que se quiera moderar para mostrarse.


-El adminsite se encuentra en admin-asdra.snoopconsulting.com es un cname de https://d2j7lnzpr7rpaq.cloudfront.net

-La base de datos se encuentra en un CloudFront de Amazon en https://d2ajhipxjgwe23.cloudfront.net/asdra-prod (asdra-dev para desarrollo) que apunta a la base original http://asdra_dev.snoopconsulting.com:5984/_utils/database.html?asdra-prod (asdra-dev-a)

-Para registrar un usuario nuevo entrar a https://auth0.com/ con la cuenta de google de Enuel (enuel.morales@snoopconsulting.com)

- Aca encontramos la lista de buckets en amazon "https://console.aws.amazon.com/s3/home?region=us-east-1#"
- Este es el bucket donde se encuentra el codigo del adminsite "aws-website-asdra-admin-eic2v"
- Este es el comando para listar los buckets por la consola "aws s3 ls s3://aws-website-asdra-admin-eic2v"
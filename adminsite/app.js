var app =  angular.module('app', ['auth0.lock', 'angular-jwt', 'ngRoute', 'ngTouch', 'ui.grid', 'ui.grid.edit', 'ui.grid.expandable', 'ui.grid.selection', 'ui.grid.pinning', 'addressFormatter', 'ngMaterial'])
                  .config(config);

config.$inject = ['$routeProvider', '$httpProvider', 'lockProvider', 'jwtOptionsProvider', 'jwtInterceptorProvider'];

function config($routeProvider, $httpProvider, lockProvider, jwtOptionsProvider, jwtInterceptorProvider) {

  // Initialization for the Lock widget
  lockProvider.init({
    clientID: AUTH0_CLIENT_ID,
    domain: AUTH0_DOMAIN
  });

  // Configuration for angular-jwt
  jwtOptionsProvider.config({
    tokenGetter: function() {
      return localStorage.getItem('id_token');
    },
    whiteListedDomains: ['localhost'],
    unauthenticatedRedirectPath: '/login'
  });

  // Add the jwtInterceptor to the array of HTTP interceptors
  // so that JWTs are attached as Authorization headers
  $httpProvider.interceptors.push('jwtInterceptor');

  $routeProvider
    .when('/', {
      controller: 'loginController',
      templateUrl: 'components/login/login.html'
    })
    .when('/login', {
      controller: 'loginController',
      templateUrl: 'components/login/login.html'
    })
    .when('/ping', {
      controller: 'pingController',
      templateUrl: 'components/ping/ping.html'
    });
}

angular.module('addressFormatter', []).filter('address', function () {
  return function (input) {
      return input.street + ', ' + input.city + ', ' + input.state + ', ' + input.zip;
  };
});

app.controller('MainCtrl', ['$scope', '$http', '$timeout', 'PersistenceService', 'uiGridConstants', '$mdDialog', function ($scope, $http, $timeout, PersistenceService, uiGridConstants, $mdDialog) {
  $scope.gridOptions = {
    expandableRowTemplate: 'expandableRowTemplateComments.html',
    expandableRowHeight: 150
  };

  // $scope.storeFile = function( gridRow, gridCol, files ) {
  //   // ignore all but the first file, it can only select one anyway
  //   // set the filename into this column
  //   gridRow.entity.filename = files[0].name;

  //   // read the file and set it into a hidden column, which we may do stuff with later
  //   var setFile = function(fileContent){
  //     gridRow.entity.file = fileContent.currentTarget.result;
  //     // put it on scope so we can display it - you'd probably do something else with it
  //     $scope.lastFile = fileContent.currentTarget.result;
  //     $scope.$apply();
  //   };
  //   var reader = new FileReader();
  //   reader.onload = setFile;
  //   reader.readAsText( files[0] );
  // };

/*
  this.name = undefined;
  this.address = undefined;
  this.phone = undefined;
  this.web = undefined;
  this.location = undefined;
  this.email = undefined;
  this.validado = undefined;
  this.autor = undefined;
*/
  $scope.refresh = function() {
    //$scope.gridApi.core.refreshRows();
    window.location.reload();
    //$scope.loadedSites();
    showMessageSuccess('Listado actualizado');
  }

  //$scope.gridOptions.rowTemplate = '';
  $scope.gridOptions.columnDefs = [
    { name: 'img', displayName: 'Foto', enableCellEdit: false, width: '4%', cellClass: 'class-site-img', cellTemplate: '<div data-id="{{row.entity._id}}" onclick="showDialog(event);" class="text-center" style="text-decoration:underline;cursor:pointer;" at-src="{{row.entity.image}}">{{ row.entity.image == "" ? "" : "Imágenes" }}</div>' },
    { name: '_id', enableCellEdit: false, width: '10%', cellClass: 'class-site-id' },
    { name: 'name', displayName: 'Nombre', sort: { direction: uiGridConstants.ASC, priority: 0 }, width: '13%' },
    { name: 'address', displayName: 'Dirección' , width: '13%' },
    { name: 'phone', displayName: 'Teléfono' , width: '10%' },
    { name: 'web', displayName: 'Web' , width: '10%' },
    { name: 'location', displayName: 'Coords' , width: '10%' },
    { name: 'email', displayName: 'Email' , width: '10%' },
    { name: 'autor', displayName: 'Autor' , width: '10%' },
    { name: 'comments_size', displayName: 'Comentarios', width: '7%' },
    { name: 'validado', displayName: 'Validado', type: 'boolean', width: '5%' },
    { name: 'grabado', displayName: 'Grabado', width: '8%' }
  ];

  $scope.msg = {};
  $scope.edit_sites = [];

  function showMessageSuccess(text) {
    var message = document.getElementsByClassName('message-success')[0];
    message.innerHTML = text;
    message.style.display = 'block';
    setTimeout(function() {
      message.style.display = 'none';
    }, 2000);
  }

  $scope.saveChanges = function(){
    $scope.edit_sites.forEach(function(site) {
      console.log(site);
        console.log(site.comments);
        site.grabado = '';

        PersistenceService.update(site)
        .then(function(){
          site.grabado = 'Guardado';
          showMessageSuccess('Cambios guardados');
        })
        .catch(function(err){
          console.error('Error :' + err + ' al grabar el site: ' + JSON.stringify(site));
          site.grabado = 'Vuelva a intentarlo';
        });
    });
    $scope.edit_sites = [];
 };

  $scope.addSiteToSaveChangesEdit = function(site) {
    delete site.$$hashKey;
      
    if ( angular.equals($scope.edit_sites.length, 0) ) {
      $scope.edit_sites.push(site);
    } else {
      var ok = false;
      angular.forEach($scope.edit_sites, function(s, key) {
        if ( angular.isDefined(s) && angular.equals(s._id,site._id) ) {
          $scope.edit_sites[key] = site;
          ok = true;
          return;
        }
      });
      if (!ok)
        $scope.edit_sites.push(site);
    }
  }

  $scope.gridOptions.onRegisterApi = function(gridApi){
    //set gridApi on scope
    $scope.gridApi = gridApi;
    console.log(gridApi);
    gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
      if ( !angular.equals(newValue, oldValue) )
        $scope.addSiteToSaveChangesEdit(rowEntity);
      
      /** console **/
        /*console.log('rowEntity: ', rowEntity);
        console.log('colDef: ', colDef);
        console.log('newValue: ', newValue);
        console.log('oldValue: ', oldValue);*/
        // $scope.msg.lastCellEdited = 'edited row id:' + rowEntity.id + ' Column:' + colDef.name + ' newValue:' + newValue + ' oldValue:' + oldValue ;
        // $scope.$apply();
      /** /console **/
    });

    gridApi.expandable.on.rowExpandedBeforeStateChanged($scope,function(scope, handler, _this){
      $scope.addSiteToSaveChangesEdit(scope.entity);
    });
  };

  $scope.expandAllRows = function() {
    $scope.gridApi.expandable.expandAllRows();
  }

  $scope.collapseAllRows = function() {
    $scope.gridApi.expandable.collapseAllRows();
  }

  // Levantamos el acceso a la base
  PersistenceService.DUMP_FILE_URL = '../ionicfinder/www/' + PersistenceService.DUMP_FILE_URL;

  $scope.loadedSites = function() {
    PersistenceService.proxyInstance()
    //.then(function(){
    //  console.log('apps.js: PersistenceService.instance().then');
    //  return PersistenceService.syncWithRemote();
    //})
    .then(function(){
          return PersistenceService.findAllDocuments('Site');
    })
    .then(function(docs){
        return docs.map(Site.buildFromDocument);
    })
    // .then(function(loadedSites){
    //     return loadedSites.filter(function(site){return site.validado || (typeof site.validado === 'undefined');});
    // })
    .then(function(sites){
        console.log('Carga de sites de db OK');
        $scope.gridOptions.data = sites;
        $scope.gridOptions.data.forEach(function(site) {
          site.comments_size = site.comments.length;
          site.subGridOptions = {
            columnDefs: [
              {name:'Nombre', field:'name'},
              {name:'Mensaje', field:'message'},
              {name:'Fecha', field:'date', cellFilter: 'date:"medium"', width: '15%'},
              {name: 'Validado', field:'validado', type: 'boolean', width: '10%'},
            ],
            data: site.comments
          }
        });
        /*sites.map(function(site){
          if (site.name == 'Preocupaciones') console.log('Antes: ' + site.validado);
        });*/
    })
    .catch(function(err){
      console.error('Error al arrancar PouchDB: ', err);
    });
  }

  $scope.loadedSites();

  showDialog = function(event) {
    var id = event.toElement.getAttribute('data-id');
    var images = event.toElement.getAttribute('at-src').split('#image#');
    var parentEl = angular.element(document.body);
    var urls_images = '';
    angular.forEach(images, function(image, key) {
      urls_images += ' <div style="display:flex; float:left; padding:5px;"><div onclick="this.nextSibling.remove();this.remove();" ng-click="removeImage(\''+id+'\', \''+image+'\')" title="Remover" class="icon-remove">X</div><img class="image-remove" src="'+image+'" style="width:200px; height:200px;"></div>';
    });

    $mdDialog.show({
      parent: parentEl,
      //targetEvent: $event,
      template:
        '<md-dialog aria-label="List dialog">' +
        '  <md-dialog-content style="padding:10px;">'+
              urls_images +
        '  </md-dialog-content>' +
        '  <md-dialog-actions>' +
        '    <button ng-click="closeDialog()" class="button button-block button-assertive">' +
        '      Cerrar' +
        '    </button>' +
        '  </md-dialog-actions>' +
        '</md-dialog>',
        locals: {
          items: $scope.items
        },
        controller: DialogController
      });

      function DialogController($scope, $mdDialog, items) {
        $scope.items = items;
        $scope.closeDialog = function() {
          $mdDialog.hide();
        }
        $scope.removeImage = function(id_site, image) {
          PersistenceService.proxyInstance()
          .then(function(){
            return PersistenceService.findById(Site, id_site);
          })
          .then(function(site){
            var arr_images = site.image.split('#image#');
            
            for (key in arr_images)
              if (arr_images[key] == image)
                arr_images.splice(key, 1);

            site.image = arr_images.join('#image#');
            PersistenceService.createOrUpdate(site);
            showMessageSuccess('Imágen borrada');
          })
          .catch(function(err){
            console.error('Error al arrancar PouchDB: ', err);
          });
        }
      }
    }

  // $http.get('https://cdn.rawgit.com/angular-ui/ui-grid.info/gh-pages/data/500_complex.json')
  //   .success(function(data) {
  //     for(i = 0; i < data.length; i++){
  //       data[i].registered = new Date(data[i].registered);
  //       data[i].gender = data[i].gender==='male' ? 1 : 2;
  //       if (i % 2) {
  //         data[i].pet = 'fish'
  //         data[i].foo = {bar: [{baz: 2, options: [{value: 'fish'}, {value: 'hamster'}]}]}
  //       }
  //       else {
  //         data[i].pet = 'dog'
  //         data[i].foo = {bar: [{baz: 2, options: [{value: 'dog'}, {value: 'cat'}]}]}
  //       }
  //     }
  //     $scope.gridOptions.data = data;
  //   });

}])

.filter('mapGender', function() {
  var genderHash = {
    1: 'male',
    2: 'female'
  };

  return function(input) {
    if (!input){
      return '';
    } else {
      return genderHash[input];
    }
  };
})

.filter('mapStatus', function() {
  var genderHash = {
    1: 'Bachelor',
    2: 'Nubile',
    3: 'Married'
  };

  return function(input) {
    if (!input){
      return '';
    } else {
      return genderHash[input];
    }
  };
})
;

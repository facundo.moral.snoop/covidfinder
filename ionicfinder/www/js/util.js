/****************************
*
* Util: Misc
*
*****************************/
var util = {

		checkConnection : function() {
		    var networkState = navigator.connection.type;

		    var states = {};
		    states[Connection.UNKNOWN]  = 'Unknown connection';
		    states[Connection.ETHERNET] = 'Ethernet connection';
		    states[Connection.WIFI]     = 'WiFi connection';
		    states[Connection.CELL_2G]  = 'Cell 2G connection';
		    states[Connection.CELL_3G]  = 'Cell 3G connection';
		    states[Connection.CELL_4G]  = 'Cell 4G connection';
		    states[Connection.CELL]     = 'Cell generic connection';
		    states[Connection.NONE]     = 'No network connection';

		    console.log('Connection type: ' + states[networkState]);
		},
		
		isConnected : function() {
//			if ( navigator.connection != undefined ) {
//				var networkState = navigator.connection.type;
//				return (networkState != Connection.NONE);
//			} else {
				return true; //por default que se comporten como si estuviéramos conectados
//			}
		}
};


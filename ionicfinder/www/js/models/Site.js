/*
 * Un Sitio registrado en ASDRA
 *
 */
function Site() {
  this.name = undefined;
  this.address = undefined;
  this.phone = undefined;
  this.web = undefined;
  this.location = undefined;
  this.email = undefined;
  this.validado = undefined;
  this.autor = undefined;
  this.hearts = 3;
  //this.town = "Colegiales";
  //this.category = "Escuela Inclusiva"
  //Estos dos atributos mantienen la identidad cuando el objeto persiste, normalmente no se deben setear a mano. Ver newId()
  this._id = undefined;
  this._rev = undefined;  
  this.hearts = 0;
  this.comments = [];
  this.image = '';
}

Site.prototype = {};

/*
 * Asigna el _id del receptor a partir de un random UUID
 * Normalmente no se debería llamar porque los métodos de persistencia lo usan si es necesario. Ver PersistenceService.create()
 * Retorna el id
 */
Site.prototype.newId = function() {
  this._id = 'Site_' + UUIDGen_uuid(32, 16).toLowerCase();
  return this._id;
}

/*
 * Retorna un boolean que indica si son iguales ( mismo valor en las properties )
 * Basada en angular.equals
 */
Site.prototype.equals = function(other) {

  function createMap() {
    return Object.create(null);
  }
  function _equals( o1, o2) {
    if (o1 === o2) return true;
    if (o1 === null || o2 === null) return false;
    if (o1 !== o1 && o2 !== o2) return true; // NaN === NaN
    var t1 = typeof o1, t2 = typeof o2, length, key, keySet;
    if (t1 === t2 && t1 === 'object') {
        keySet = createMap();
        for (key in o1) {
          if (key.charAt(0) === '$' || key === '_id' || key === '_rev' || angular.isFunction(o1[key])) continue;
          if (!_equals(o1[key], o2[key])) return false;
          keySet[key] = true;
        }
        for (key in o2) {
          if (!(key in keySet) &&
              key.charAt(0) !== '$' &&
              key !== '_id' &&
              key !== '_rev' &&
              angular.isDefined(o2[key]) &&
              !angular.isFunction(o2[key])) return false;
        }
        return true;
    }
    return false;
  }
  return _equals(this,other);
}

/*
 * "copy constructor"
 */
Site.buildFromDocument = function(document) {
  var newborn = Object.assign(new Site(), document);
  return newborn;
}

/*
 * "copy constructor"
 * Toma el _id del document y le agrega "Site_" como prefijo. Si no tiene, asigna uno random via newId()
 */
Site.buildFromBootstrap = function(document) {
  var newborn = Site.buildFromDocument(document);
  delete newborn._metadata;
  if ( !newborn._id ) {
    newborn.newId();
  } else {
    newborn._id = 'Site_' + newborn._id;
  }
  return newborn;
}

// Now, logic

Site.prototype.getImages = function(){
  return this.image.split('#image#');
};

Site.prototype.getComments = function(){
  return this.comments;
};

Site.prototype.getHearts = function(){
  return this.hearts;
};

Site.prototype.getLat = function(){
	return this.location[1];
};

Site.prototype.getLng = function(){
	return this.location[0];
};

Site.prototype.hasWeb = function(){
	return (typeof this.web === "string") && this.web && this.web.length > 0;
};

Site.prototype.getWebURL = function(){
	if ( this.hasWeb()) {
		var url = this.web;
		if (url.indexOf('://') === -1) {
		  url = 'http://' + url;
		}
		return url;
	} else {
		return undefined;
	}
};

Site.prototype.hasLocation = function(){
	return (typeof this.location === "object") && this.location && this.location.length == 2;
};

Site.prototype.hasPhone = function(){
	return (typeof this.phone === "string") && this.phone && this.phone.length > 0;
};

//Retorna el Phone sin espacios
Site.prototype.getPhone = function(){
	return this.phone.replace(/\s/g, '')
};


//Abre el navegador nativo hacia el sitio
Site.prototype.navigate = function(){
  console.log('navigate');

	// Uso las coordenadas que son más precisas. Cuando abra el Apple/Google Maps, 
	// estos hacen la geoloc inversa y muestran la dirección al usuario

	var destLatLng = '' + this.location[1] + ',' + this.location[0];
	console.log('navigate. destLatLng: ' + JSON.stringify(destLatLng));

	if ( typeof device !== 'undefined') {

	  if ( device.platform === "iOS" ) {
	    //No sé como validar que pueda abrir google maps ( por ahí no está instalado ), así que abro el default
	    // window.open('http://maps.apple.com/?daddr=' + this.address.replace(/\s/g, '+') + '' , '_system');
	    window.open('http://maps.apple.com/?daddr=' + destLatLng , '_system');

	  } else if (device.platform === "Android" ) {
	    // window.open('http://maps.google.com/maps?daddr=' + this.address.replace(/\s/g, '+') + '' , '_system');
	    window.open('http://maps.google.com/maps?daddr=' + destLatLng , '_system');
	  } else {
	    console.log("No sé como manejar la plataforma: " + device.platform);
	  }

	} else {

	  // No sé en qué plataforma estoy, tiro el URL de google maps
	  window.open('http://maps.google.com/maps?daddr=' + destLatLng , '_system');

	  //Ejemplos para probar en el debugger.
	  //Estos mismos URLs son los que termina abriendo el plugin            

	  //if ( iOS )

	  // window.open('http://maps.apple.com/?daddr=' + this.address.replace(/\s/g, '+') + '&saddr=Calle 41 Nro 1614, La Plata'.replace(/\s/g, '+') , '_system');
	  // window.open('http://maps.apple.com/?daddr=' + this.address.replace(/\s/g, '+') + '&saddr=-34.602219,-58.409438' , '_system');
	  //No especifico la saddr para que tome la ubicación actual
	  // console.log('dst addr: ' + this.address.replace(/\s/g, '+'));
	  // window.open('http://maps.apple.com/?daddr=' + this.address.replace(/\s/g, '+') + '' , '_system');

	  //Este es el URL que abre Google Maps si está instalado en el cel/tablet
	  // window.open('comgooglemaps://?saddr=&daddr=' + this.address.replace(/\s/g, '+'), '_system');

	  // } else if ( android ) {

	  // window.open('http://maps.google.com/maps?daddr=' + this.address.replace(/\s/g, '+') + '' , '_system');

	  //}
              
    }
}

//Abre la app nativa para llamar
Site.prototype.call = function(){
  console.log('tel. site.phone: ' + this.getPhone());
	window.open('tel:' + this.getPhone(), '_system');
}


//Abre browser externo a la app
Site.prototype.browse = function(){
	console.log('web page');
	//web page
	//https://github.com/apache/cordova-plugin-inappbrowser/blob/3b762124979a5fd615247bcbaeea8d652d939990/docs/window.open.md
	var url = this.getWebURL();
	console.log('Por abrir:' + url);
	window.open(url, '_system', 'location=yes');
}

Site.prototype.canShare = function(){
  return typeof window.plugins !== 'undefined' && typeof window.plugins.socialsharing !== 'undefined';
}

Site.prototype.share = function(){
  if ( typeof window.plugins === 'undefined' || typeof window.plugins.socialsharing === 'undefined' ) {
    console.log("Falta cargar el plugin de socialsharing");
  } else {
    Social_shareNative(this.name,this.getWebURL());
  }
}

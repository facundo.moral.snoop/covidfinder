//Funcionalidad y parches google maps plugin


//Singleton del mapa
var map;

//Ubicación actual de acuerdo al mapa
var currentPosition;

//Como el mapa no se "autoexpande", lo acomodo cuando cambia la orientación del disp.
window.addEventListener("orientationchange", function() {
  fixMapHeight();
}, false);


//Reacomoda el mapa.
//OJO que depende de la estructura donde está metido, por ejemplo toma en cuenta si hay una search-bar en la misma view
function fixMapHeight() {
  //Definido en el template de ionic donde mostramos la app
  var mapdiv = document.getElementById("map_canvas");
  var ionContent = mapdiv.parentNode.parentNode;
  var searchBarHeight = 0;
  if ( document.getElementById("search-bar") ) {
    searchBarHeight = document.getElementById("search-bar").clientHeight;
  } 

  var newHeight = ionContent.clientHeight - searchBarHeight;

  mapdiv.style.height = "" + newHeight + "px";
  console.log("resized to: " + ("" + newHeight + "px"));
}

//Re-engancha el mapa al div, poniendo background = 0,0,0,0 a todos los parents
function reattachMap(map,div) {
  if (!isDom(div)) {
    console.log("div is not dom");
    return map;
  } else {
    map.set("div", div);

    while(div.parentNode) {
      div.style.backgroundColor = 'rgba(0,0,0,0)';
      div = div.parentNode;
    }

    return map;
  }
}

function isDom(element) {
  return !!element &&
         typeof element === "object" &&
         "getBoundingClientRect" in element;
}

//Crea un map en el div map_canvas
//Le setea el centro en Snoop BsAs, se supone que después la app lo va a mover a la ubicación actual
function createMap() {

    if ( typeof plugin === "undefined" ) {
      console.log("El plugin map es undefined");
        return;
    }
    console.log('Creating map');
    var mapDiv = document.getElementById("map_canvas");
    var SNOOP_BSAS = new plugin.google.maps.LatLng(-34.602219,-58.409438);
    var VILLA_ELISA = new plugin.google.maps.LatLng(-34.8605537,-58.1084798);
    // Initialize the map plugin
    map = plugin.google.maps.Map.getMap(mapDiv,{
      'backgroundColor': 'white',
      'mapType': plugin.google.maps.MapTypeId.ROADMAP,   // otra es HYBRID
      'controls': {
        'compass': true,
        'myLocationButton': true,
        'indoorPicker': true,
        'zoom': true  // Only for Android. En iOS se hace con pinch
      },
      'gestures': {
        'scroll': true,
        'tilt': true,
        'rotate': true,
        'zoom' : true
      },
      'camera': {
        'latLng': SNOOP_BSAS,
        // 'tilt': 30,
        'zoom': 4,
        'bearing': 0
      }
    });

    map.on(plugin.google.maps.event.MAP_READY, onMapInit);

    map.on(plugin.google.maps.event.CAMERA_CHANGE, onMapCameraChanged);
}

function onMapCameraChanged(position) {
  // console.log(JSON.stringify(position));
  //FIXME
  //model.saveCurerntLoc(position) o algo así
  currentPosition = position;
}

//El mapa está creado, le ajusto la altura y lo reposiciono a la ubicación actual
function onMapInit(map) {
    fixMapHeight();
    map.getMyLocation(onGetMyLocationSuccess, onGetMyLocationError);
    //FIXME acá resolve a algún deferred del que estén colgados los que esperan el mapa
    console.log('map ready');
}

var onGetMyLocationSuccess = function(location) {
    console.log('ubicación actual: ' + JSON.stringify(location));

    map.animateCamera({
      target: new plugin.google.maps.LatLng(location.latLng.lat, location.latLng.lng),
      zoom: 14,
      duration: 5000
    }, function() {
      console.log("The animation is done.");
    });
};

var onGetMyLocationError = function(err) {
    console.log('No pude obtener la ubicación actual: ' + JSON.stringify(err));
};

var asdra_marker_icon;
var ASDRA_MARKER_ICON_URL = 'img/logo-asdra.png';

var getAsdraIconDataURL = function (){
		var deferred = _map_$q.defer();
    if ( asdra_marker_icon ) {
			deferred.resolve(asdra_marker_icon);      
    } else {
      var canvas = document.createElement('canvas');
      canvas.width = 32;
      canvas.height = 28;
      var context = canvas.getContext('2d');
      var img = new Image();
      img.onload = function () {
        context.drawImage(img, 0, 0, 32, 28);
        asdra_marker_icon = canvas.toDataURL(); 
        deferred.resolve(asdra_marker_icon);
      }
      img.onerror = function(e){
        console.log('createIconMarker: error: ' + e);
        deferred.reject(e);
      }
      img.src = ASDRA_MARKER_ICON_URL;
    }
		return deferred.promise;
	};


var addSitesToMap = function ( sites, $scope ) {
  console.log("addSitesToMap sites.length: " + sites.length);

  if ( map ) {
    // map.clear();

    var infoClick = function(marker) {
      // console.log("InfoWindow clicked");
      $scope.openActionSheet(marker);
    };
    var markerClick = function(marker) {
      // console.log("Marker clicked");
    };

    getAsdraIconDataURL()
        .then(function(iconDataUrl){
          // Las necesito para que c/u haga referencia a un Site distinto
        /*jshint loopfunc: true */
          for(var i in sites) {
            var site = sites[i];
            if ( typeof site.location === "undefined" || 
                !site.location || site.location === null || site.location == "null" ) {
              console.log("Skipping one unlocated marker. location = " + JSON.stringify(site.location));
              continue;
            }
            if ( typeof site.location === "string" ) {
              try {
                site.location = JSON.parse(site.location);
                if ( typeof site.location !== "object" || site.location.length != 2 ) {
                  console.log('Después de parsear una location, no es un array sino: ' + JSON.stringify(site.location));
                  continue;
                }
              } catch (e) {
                console.log('Exception al parsear la location ' + site.location + ' error: ' + JSON.stringify(e));
                continue;                
              }
            }

            //add marker to map
            if ( typeof plugin !== "undefined" ) {
              var markerPos = new plugin.google.maps.LatLng(site.location[1],site.location[0]);      
              var markerOpts = {
                position: markerPos,

                title: site.name + '\n' +
                  (typeof site.address === "undefined" ? '' : ( 'Dirección: ' + site.address + '\n')) + 
                  (typeof site.phone === "undefined" ? '' : ( 'Tel: ' + site.phone  + '\n' )),

                snippet: 'Clickee para más acciones\n',
                markerClick: markerClick,
                infoClick: infoClick, 
                site : site,
                icon : iconDataUrl
              };
              map.addMarker(markerOpts, function(marker) {
              });
          }
        }

        })
        .catch(function(err){
          console.err('Error al agregar un marker: ' + err);
        });
  }
};




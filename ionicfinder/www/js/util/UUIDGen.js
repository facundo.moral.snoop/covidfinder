/*!

Tomado de pouchdb sources
Math.uuid.js (v1.4)
http://www.broofa.com
mailto:robert@broofa.com

Copyright (c) 2010 Robert Kieffer
Dual licensed under the MIT and GPL licenses.
*/

/*
 * Generate a random uuid.
 *
 * USAGE: UUIDGen_uuid(length, radix)
 *   length - the desired number of characters
 *   radix  - the number of allowable values for each character.
 *
 * EXAMPLES:
 *   // No arguments  - returns RFC4122, version 4 ID
 *   >>> UUIDGen_uuid()
 *   "92329D39-6F5C-4520-ABFC-AAB64544E172"
 *
 *   // One argument - returns ID of the specified length
 *   >>> UUIDGen_uuid(15)     // 15 character ID (default base=62)
 *   "VcydxgltxrVZSTV"
 *
 *   // Two arguments - returns ID of the specified length, and radix.
 *   // (Radix must be <= 62)
 *   >>> UUIDGen_uuid(8, 2)  // 8 character ID (base=2)
 *   "01001010"
 *   >>> UUIDGen_uuid(8, 10) // 8 character ID (base=10)
 *   "47473046"
 *   >>> UUIDGen_uuid(8, 16) // 8 character ID (base=16)
 *   "098F4D35"
 */
var UUIDGen_chars = (
  '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
  'abcdefghijklmnopqrstuvwxyz'
).split('');

function UUIDGen_getValue(radix) {
  return 0 | Math.random() * radix;
}

function UUIDGen_uuid(len, radix) {
  radix = radix || UUIDGen_chars.length;
  var out = '';
  var i = -1;

  if (len) {
    // Compact form
    while (++i < len) {
      out += UUIDGen_chars[UUIDGen_getValue(radix)];
    }
    return out;
  }
  // rfc4122, version 4 form
  // Fill in random data.  At i==19 set the high bits of clock sequence as
  // per rfc4122, sec. 4.1.5
  while (++i < 36) {
    switch (i) {
      case 8:
      case 13:
      case 18:
      case 23:
        out += '-';
        break;
      case 19:
        out += UUIDGen_chars[(UUIDGen_getValue(16) & 0x3) | 0x8];
        break;
      default:
        out += UUIDGen_chars[UUIDGen_getValue(16)];
    }
  }

  return out;
}

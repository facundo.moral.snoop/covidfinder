(function() {
  'use strict';

  angular
    .module('MediaService', []);
  angular
    .module('MediaService')
    .factory('MediaService', MediaService);

  function MediaService($q, $cordovaImagePicker) {

    var MediaService = {
      getAudio: getAudio,
      getVideo: getVideo,
      getImage: getImage
    }

    return MediaService;

    function getAudio() {
      var deferred = $q.defer();
      navigator.device.capture.captureAudio(
        function(result) {
          console.log(angular.toJson(result));
          var key = new Date().getTime() + result[0].localURL.substr(result[0].localURL.lastIndexOf('/') + 1);
          var attachment = {
            'type':'audio',
            'content':{
              'id': key,
              'LocalFileURL': result[0].localURL.split(':/')[1],
              'fullPath': result[0].fullPath.split(':')[1],
              'state':''
            }
          };
          deferred.resolve(attachment);
        },
        function(error) {
          console.error(error);
          deferred.reject(error);
        }, {
          limit: 1
        }
      );
      return deferred.promise;
    }

    function getVideo() {
      var deferred = $q.defer();
      navigator.device.capture.captureVideo(
        function(result) {
          console.log(angular.toJson(result));
          var key = new Date().getTime() + result[0].localURL.substr(result[0].localURL.lastIndexOf('/') + 1);
          var attachment = {
            'type':'video',
            'content':{
              'id': key,
              'LocalFileURL': result[0].localURL.split(':/')[1],
              'fullPath': result[0].fullPath.split(':')[1],
              'state':''
            }
          };
          deferred.resolve(attachment);
        },
        function(error) {
          console.error(error);
          deferred.reject(error);
        }, {
          limit: 1
        }
      );
      return deferred.promise;
    }

    function getImage() {
      var deferred = $q.defer();
      var options = {
        maximumImagesCount: 1
      };
      $cordovaImagePicker.getPictures(options)
        //<<<<<<TODO: DEBERIAMOS VOLVER A ESTA IMPLEMENTACION CUANDO QUITEMOS LAS IMAGENES DE LOS HALLAZGOS
        //         .then(function(url) {
        //           console.log('Image URI: ' + url);
        //           console.log(angular.toJson(url));
        //           var result = url[0];
        //           var k = result.substr(result.lastIndexOf('/') + 1) + new Date().getTime();
        //           var attachment = {
        //             'Type': 'Image',
        //             'id': k,
        //             'LocalFileURL': result,
        //             'fullPath': result,
        //             'state': ''
        //           };
        //           deferred.resolve(attachment);
        // =======
        .then(function(result) {
          console.log('Image URI: ' + result);
          deferred.resolve(result);
          //>>>>>>> 39f5fbf01f7c6b6ab830b7d0ea9e92f3ecfb0de2
        }).catch(function(error) {
          console.error(error);
          deferred.reject(error);
        });
      return deferred.promise;
    }


  }
})();

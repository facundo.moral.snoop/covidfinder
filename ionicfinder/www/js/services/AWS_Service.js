angular.module('ionicfinder')
.service('AWS_Service', function($q, $window, $timeout, $interval, $http, FileReaderService) {
    if (!$window.localStorage.media) {
      $window.localStorage.media = [];
    }

    var AWS_Service = {
      getImage: getImage,
      uploadFile: uploadFile,
      getURL: getURL,
      setReference: setReference
    }

    $timeout(function() {
      managePendingJobs();
    });

    return AWS_Service;

    function managePendingJobs() {
      getNextPendingJob().then(function(index) {
        console.log("[AWS_Service] Trabajo pendiente encontrado: " + JSON.stringify($window.localStorage.media[index]));
        upload($window.localStorage.media[index]);
        managePendingJobs();
      }).catch(function(err) {
        console.log('[AWS_Service] '+ err);
      });
    }

    function getNextPendingJob() {
      var deferred = $q.defer();

      // var index = false;
      // var i = 0;
      // while (!index && i < $window.localStorage.media.length) {
      //   if ($window.localStorage.media[i].state === 'pending') {
      //     index = i;
      //     deferred.resolve(index);
      //   } else {
      //     i++;
      //   }
      // }
      // if (!index) {
      //   deferred.reject("Ningun trabajo pendiente");
      // }
      var index = [];
      angular.forEach($window.localStorage.media, function(job, i) {
        if (job.content.id === 'pending') {
          index.push(i);
        }
      });
      if (index.length > 1) {
        console.log("[AWS_Service] Trabajos duplicados?");
      }
      if (index.length > 0) {
        deferred.resolve(index[0]);
      } else {
        deferred.reject("No se encontro coincidencia");
      }


      return deferred.promise;
    }

    function getLocation(id) {
      var index = [];
      angular.forEach($window.localStorage.media, function(job, i) {
        if (job.content.id === id) {
          index.push(i);
        }
      });
      if (index.length > 1) {
        console.log("[AWS_Service] Trabajos duplicados?");
      }
      if (index.length > 0) {
        return index[0];
      } else {
        return false
      }
    }

    function setReference(id, ref) {
      var index = getLocation(id);
      if (index) {
        $window.localStorage.media[index].ref = ref;
      } else {
        console.log("[AWS_Service] Error item no encontrado");
      }
      console.log("[AWS_Service] Media Storage: " + JSON.stringify($window.localStorage.media));
    }

    function checkConecction() {
      var Timer = $interval(function() {
        //FIXME: hacer ping al server s3 o azure
        console.log("[AWS_Service] Probando la conexion a internet");
        $http.get("https://www.google.com").then(function() {
          console.log("[AWS_Service] Status server s3: Online.");
          $interval.cancel(Timer);
          managePendingJobs();
        }).catch(function(error) {});
      }, 5000);
    }

    function getURL(id) {
      var index = getLocation(id);
      var url = false;
      if ($window.localStorage.media[index].content.fileURL) {
        url = $window.localStorage.media[index].content.fileURL;
      }
      return url;
    }

    function pushOnList(attach) {
      var result = getLocation(attach.content.id);
      if (!result) {
        $window.localStorage.media.push(attach);
        return true
      } else {
        return false
      }
    }

    function uploadFile(file) {
      console.log('[AWS_Service] uploadFile: ' + file);
      var deferred = $q.defer();
      var fr = new FileReader();

      fr.onload = fileReaderSuccess;
      fr.readAsArrayBuffer(file);

      function fileReaderSuccess() {
        var s3 = new AWS.S3({
          endpoint: 'https://s3-sa-east-1.amazonaws.com',
          accessKeyId: 'AKIAJQMS2G7TA5ZQ5M6A',
          secretAccessKey: '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',
          region: 'sa-east-1'
        });

        var buffer = fr.result;
        var view = new Int8Array(buffer);

        var params = {
          Bucket: "asdraattachments",
          Key: file.name,
          Body: view,
          ContentType: file.type
        };

        s3.upload(params, function (err, data) {
          if (err) {
            console.log("Error al cargar la info: " + err);
            deferred.reject(err);
          } else {
            console.log("La info se cargó bien: " + JSON.stringify(data));
            deferred.resolve(data.Location);
          }
        });
      }

      return deferred.promise;
    }

    function getImage() {
      var s3 = new AWS.S3({
        endpoint: 'https://s3-sa-east-1.amazonaws.com',
        accessKeyId: 'AKIAJQMS2G7TA5ZQ5M6A',
        secretAccessKey: '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',
        region: 'sa-east-1'
      });

      return s3.getSignedUrl(
        "getObject",
        {
          Bucket: "asdraattachments",
          Key: "output.ext",
          ResponseContentType: "text/plain"
        }
      );
    }

    /*function upload(attachment) {
      console.log('[AWS_Service] attachment: '+ JSON.stringify(attachment));
      var deferred = $q.defer();
      var index = false;
      //Trato de colocar el trabajo en la cola/lista
      //pushOnList devuelve falso si el archivo ya estaba cargado en la cola/lista, lo cual implica que era un trabajo pendiente
      if (pushOnList(attachment)) {
        //Si entro aca es porque pushOnList nos avisa que el attachment no estaba cargado como trabajo pendiente, y que ya lo cargo, por lo que podemos obtener su ubicacion.
        index = getLocation(attachment.content.id);
      }

      console.log("[AWS_Service] Preparando credenciales..");
      AWS.config.update({
        accessKeyId: 'AKIAJQMS2G7TA5ZQ5M6A',
        secretAccessKey: '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',
        region: 'sa-east-1'
      });
      var s3 = new AWS.S3({
        endpoint: 'https://s3-sa-east-1.amazonaws.com'
      });
      console.log("[AWS_Service] Preparando archivo..");


      if (index) {
        var fileURL = encodeURI($window.localStorage.media[index].content.fullPath);
        // $http.get(fileURL).then(function(result) { CORROMPE LOS ARCHIVOS
        console.log('[AWS_Service] Archivo local: '+ $window.localStorage.media[index].content.fullPath);
        FileReaderService.readFileAsync($window.localStorage.media[index].content.fullPath)
        .then(function(result) {
          console.log("[AWS_Service] Payload listo..");
          var params = {
            Bucket: 'asdraattachments',
            Key: $window.localStorage.media[index].content.id,
            Body: result
          };

          console.log("[AWS_Service] Subiendo archivo..");
          s3.upload(params, function(err, data) {
            var result = err ? 'ERROR' : 'UPLOADED';
            if (result === 'UPLOADED') {
              console.log("[AWS_Service] Carga finalizada con exito: " + data.Location);
              $window.localStorage.media[index].content.fileURL = data.Location;
              $window.localStorage.media[index].content.state = 'done';
              deferred.resolve();
            } else if (result === 'ERROR') {
              console.error("[AWS_Service] Fallo la carga del archivo");
              $window.localStorage.media[index].content.state = 'pending';
              //checkConecction();
              deferred.reject();
            }
          });

        }).catch(function(err) {
          console.log("[AWS_Service] Error al recuperar el archivo.");
          deferred.reject();
        });
      } else {
        console.log("[AWS_Service] Error en almacenamiento local");
        deferred.reject();
      }
      return deferred.promise;

      // delete attachment.LocalFileURL;

      // console.log("Obteniendo lista de archivos en edwardsblobs");
      // s3.listObjects({
      //   Bucket: 'edwardsblobs'
      // }, function(error, data) {
      //   if (error) {
      //     console.log(error); // an error occurred
      //   } else {
      //     console.log(data); // request succeeded
      //   }
      // });
      //
    }*/

  });

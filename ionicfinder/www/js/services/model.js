angular.module('ionicfinder.model', [])

/**
 * Modelo de la app, maneja los sitios
 *
 */ 
.service('finderModelService', function(PersistenceService, $q, $rootScope){

    var sites = [];

    /**
     * Carga el sites con los sitios validados solamente si sites es vacío.
     * 
     * Retorna una promise con los sitios
     */ 
    this.loadSites = function(){

        var deferred = $q.defer();

    	if ( sites.length === 0 ) {

            PersistenceService.instance()
            .then(function(p){
                return p.findAllDocuments('Site');
            })
            .then(function(docs){
                return docs.map(Site.buildFromDocument);
            })
            .then(function(loadedSites){
                return loadedSites.filter(function(site){return site.validado || (typeof site.validado === 'undefined');});
            })
            .then(function(validatedSites){
                console.log('Carga de sites de db OK. validatedSites.length: ' + validatedSites.length);
              	if ( sites.length === 0 ) {
                    sites = validatedSites.sort(function( a, b ) {
                        return b.hearts - a.hearts;
                    });
                } else {
                    console.log('[finderModelService] Ya hay sitios cargados, no los modifico. Raro xq el reload lo puso a [], ¿se metió otro loadSites?');
                }
                deferred.resolve(sites);
            })
            .catch(function(err){
              console.log('Error:' + err);
              deferred.reject(err);
            });
    	} else {
    		deferred.resolve(sites);
    	}
		return deferred.promise;
    };

    this.reload = function(){
        console.log('reload...');
    	sites = [];
		return this.loadSites()
        .then(function(){
            $rootScope.$broadcast('refreshSites');
        })
        .catch(function(){
            console.error('finderModelService reload: error');
        });
    };


    /**
     * Nada tiene comentarios
     * 
     */ 
    this.loadPartial = function(index){
      return this.loadSites()
      .then( function (datos) {
        var result = [];
        var limite = 20 + index;

        if ( limite <= datos.length ) {
            for(var x=index; x<limite; x++){
                result.push(datos[x]);
            }
        } else if(index<datos.length){
            for(var x =index; x<datos.length; x++){
                result.push(datos[x]);
            }
        } else {
            console.log('[finderModelService] No agrego nada.')
        }
        return result;
      })
    }

    this.addSite = function(siteObj){
        var newSite = Site.buildFromDocument(siteObj);

        return PersistenceService.instance()
        .then(function(p){
            return p.create(newSite);
        })
        .then(function(site){
            console.log('site creado: ' + site);
            return site;
        });
    };

    this.addPictures = function(site){
        var editSite = Site.buildFromDocument(site);

        return PersistenceService.instance()
        .then(function(p){
            return p.update(editSite);
        })
        .then(function(){
            return self.reload();
        });
    };

    this.addComment = function(sites, comment){

        var getSite = function getSite (sites, id) {
            return sites.find( function ( site ) {
                return site._id === id;
            });
        };
        
        var siteObj  = getSite(sites, comment.id_site);
        console.log('Comentario model: '+siteObj.name);
        var editSite = Site.buildFromDocument(siteObj);
        var aux_comment  = {name: comment.name, date: new Date(), message: comment.message, validado: false};
        editSite.comments.push(aux_comment);

        return PersistenceService.instance()
        .then(function(p){
            return p.update(editSite);
        })
        .then(function(aux_comment){
            console.log('comentario agregado: ' + aux_comment);
            return aux_comment;
        })
        .then(function(){
            return self.reload();
        });
    };

    this.addHearts = function(siteObj, num){
        var editSite = Site.buildFromDocument(siteObj);

        return PersistenceService.instance()
        .then(function(p){
            editSite.hearts = num;
            return p.update(editSite);
        })
        .then(function(site){
            console.log('site modificado: ' + site);
            return site;
        })
        .then(function(){
            return self.reload();
        });
    };

    var self = this;

    $rootScope.$on('sitesLoaded', function(){
        console.log('finderModelService sitesLoaded handler');
        self.reload();
    });

});

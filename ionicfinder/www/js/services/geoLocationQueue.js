angular.module('geoLocationQueue', [])

.service('geoLocationQueueService', function(){

  var queue = [];

  var resultsCallback = undefined;

  var running = false;

  this.init = function(fx) {
    console.log("geoLocationQueueService callback es un :  " + typeof fx);
    resultsCallback = fx;
  };

  this.push = function(address) {
    console.log("geoLocationQueueService pushing:  " + JSON.stringify(address));
    queue = [];
    queue.push(address);
    this.runQueue();
  };

  this.runQueue = function() {
    console.log("geoLocationQueueService runQueue");
    if ( queue.length === 0 ) {
      console.log("geoLocationQueueService queue.length === 0");
      return;
    }

    if ( typeof resultsCallback === "undefined" ) {
      console.log("geoLocationQueueService typeof resultsCallback === 'undefined'");
      return;
    }

    if ( running ) {
      console.log("geoLocationQueueService already running");
      return;
    }
    running = true;

    var searchAddr = queue.pop();
    console.log("geoLocationQueueService searchAddr: " + JSON.stringify(searchAddr));

    if ( typeof plugin !== "undefined" ) {
      var geoRequest = {
        'address': searchAddr
      };
      console.log("geoLocationQueueService por llamar al geocode: " + JSON.stringify(geoRequest));

      plugin.google.maps.Geocoder.geocode(geoRequest, function(results, error) {
          if ( typeof error !== "undefined") {
            console.log("geoLocationQueueService error: " + JSON.stringify(error));            
          }
          var foundAddr = [];
          for(var i in results) {
              var result = results[i];

              var addressArray = [
                result.thoroughfare || "",
                result.subThoroughfare || "",
                result.locality || "",
                result.subAdminArea || "",
                result.adminArea || "",
                // result.postalCode || "",
                result.country || ""];

              for (var i = 0; i < addressArray.length; i++) {
                if (addressArray[i] == "") {         
                  addressArray.splice(i, 1);
                  i--;
                }
              }

              var addressText = addressArray.join(", ");
                
              //Si viene el detalle en iOS, tomo esto que es una mejor descripción
              if ( result.extra && result.extra.address && result.extra.address.FormattedAddressLines) {
                addressText = result.extra.address.FormattedAddressLines.join(", ");
              }  
              console.log("searchAddress. por agregar: " + JSON.stringify(addressText));
              // result.position.lat y result.position.lng son floats
              foundAddr.push({ "streetAddress" : addressText, "position" : result.position });
        }
        resultsCallback(foundAddr);
        running = false;
      });            
    } else {
      console.log("No plugin - debug");
      //simulo geocoding q es asynch
      setTimeout( function() {
        var foundAddr = [];
        foundAddr.push({ "streetAddress" : "Laprida 755, normalizado", "position" : { "lat" : 1, "lng" : 2}});
        resultsCallback(foundAddr);
        running = false;
      }, 100);
    }

    // cuando termina de "andar", que se fije si hay otro y ahí enganche.
    // Si no que termine, en el próximo push va a arrancar
    if ( queue.length > 0 ) {
      this.runQueue();
    }
  }



});




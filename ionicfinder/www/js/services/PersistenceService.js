/**
 * Maneja la persistencia sobre PouchDB
 *
 *
 */
function PersistenceService($http, $httpParamSerializer, $q, $rootScope) {

  var PersistenceService = {
    instance: instance,
    proxyInstance : proxyInstance,
    registerChangeListener: registerChangeListener,
    resetLocalData: resetLocalData,
    deleteLocalData: deleteLocalData,
    pouch: getPouch,
    syncWithRemote: syncWithRemote,
    loadBootstrap: loadBootstrap,
    cancelSyncWithRemote: cancelSyncWithRemote,
    findAllDocuments: findAllDocuments,
    findAll: findAll,
    findById: findById,
    create: create,
    update: update,
    createOrUpdate: createOrUpdate,
    upsert : upsert,
    remove: remove
  };

  //Defaults: Base en Azure Snoop
  PersistenceService.DB_NAME = 'asdra-dev-a';
  PersistenceService.REMOTE_DB_URL = 'https://admin:TlccaS.1986@d2ajhipxjgwe23.cloudfront.net' + '/' + PersistenceService.DB_NAME; 
  PersistenceService.DUMP_FILE_URL = './dbdumps/asdra-dev-a-azure.dump';
  PersistenceService.BOOTSTRAP_FILE = './bootstrap.json';
  PersistenceService.CHANGE_UPDATE_EVENT = 'change.update';
  
  PersistenceService.NEW_CHANGES = 'NEW_CHANGES';
  PersistenceService.NEW_WALL_POST = 'NEW_WALL_POST';

  //env.js para hacer override para test/prod otros
  config_env_persistenceService(PersistenceService);
  
  // Para usar si queremos un CouchDB local
  // PersistenceService.REMOTE_DB_URL =
  //     'http://127.0.0.1:5984' +
  //     '/' + PersistenceService.DB_NAME;


  var self = PersistenceService;

  var pouch;

  var pouchRep;

  return PersistenceService;

  /////////////

  /**
   *
   * Se asegura de que la pouch esté creada a inicializada
   *
   * @return una promise
   */
  function instance() {
    var deferred = $q.defer();
    if (pouch) {
      deferred.resolve(self);
    } else {
      pouch = new PouchDB(self.DB_NAME, {
        // ajax: {
        //   timeout: 10000
        // }
      });

      console.log(self.DB_NAME + ' created, using adapter: ' + pouch.adapter);

      pouch.info().then(console.log.bind(console));

      // Si queremos borrar la base para forzar la recarga, desde la consola JS:
      // indexedDB.deleteDatabase("_pouch_ + PersistenceService.DB_NAME);
      // Si la crea con WebSQL:
      // rm  ~/Library/Application\ Support/Google/Chrome\ Canary/Default/databases/Databases.db

      // Deshabilitado xq no me interesa procesar los changes que genero localmente, sino los de sync
      // self.registerChangeListener();
      deferred.resolve(self);
    }
    return deferred.promise;
  }

  /**
   *
   * Se asegura de que la pouch esté creada a inicializada
   *
   * @return una promise
   */
  function proxyInstance() {
    var deferred = $q.defer();
    if (pouch) {
      deferred.resolve(self);
    } else {
      pouch = new PouchDB(self.REMOTE_DB_URL, {
        // ajax: {
        //   timeout: 10000
        // }
      });

      console.log(self.REMOTE_DB_URL + ' created, using adapter: ' + pouch.adapter);

      pouch.info().then(console.log.bind(console));

      // Si queremos borrar la base para forzar la recarga, desde la consola JS:
      // indexedDB.deleteDatabase("_pouch_ + PersistenceService.DB_NAME);
      // Si la crea con WebSQL:
      // rm  ~/Library/Application\ Support/Google/Chrome\ Canary/Default/databases/Databases.db

      // Deshabilitado xq no me interesa procesar los changes que genero localmente, sino los de sync
      // self.registerChangeListener();
      deferred.resolve(self);
    }
    return deferred.promise;
  }
  function registerChangeListener() {
    // Para procesar los changes:
    // OJO Estos también se generan para cambios locales!!!!!!!!!
    // Si solamente me interesan los remotos, usar el del sync 
    // A list of changes made to documents in the database, in the order they were made.
    
    var changesListener = pouch.changes({
        live: true, // Will emit change events for all future changes until cancelled.
        include_docs: true,
        conflicts: true, // Include conflicts.
        attachments: true, // Include attachments.
        binary: true, // Return attachment data as Blobs/Buffers, instead of as base64-encoded strings.            
        since: 'now'
      })
      .on('change', function(change) {

        //change.changes : array de { rev : <el id> }
        //change.doc : el doc que cambió
        //change.id : el id del doc que cambió ( change.id === change.doc._id )
        //change.seq :

        // Si es un delete:
        //change.deleted : true

        /* TODO analizar esto:
        Keep in mind that this "update vs. create" test is not foolproof
        (what happens if a 2- document is the first version that gets synced?
        what happens if two conflicting 1- revisions are synced?),
        but it will match the old behavior.
        */

        /* TODO Analizar como detecta que es lo que cambió y lo updatea en:
        http://pouchdb.com/2015/02/28/efficiently-managing-ui-state-in-pouchdb.html
        */

        if (change.deleted) {
          console.debug('change.deleted: ' + JSON.stringify(change));
          // console.debug('change.deleted: '); // + JSON.stringify(change));
        } else if (change.changes.length === 1 && /^1-/.test(change.changes[0].rev)) {
          console.debug('change.create: ' + JSON.stringify(change)); //  

          if (/^WallPost_/.test(change.id)) {
            $rootScope.$broadcast(PersistenceService.NEW_WALL_POST, change.doc);
            console.debug('change.create: ' + PersistenceService.NEW_WALL_POST); //  
          }
        } else {
          console.debug('change.update: '+ JSON.stringify(change)); 
          $rootScope.$emit(PersistenceService.CHANGE_UPDATE_EVENT, change);
        }

      });
  }

  /**
   * Retorna la pouch usada. Usar con cuidado
   *
   * @return un Promise que se resuelve con la pouch
   */
  function getPouch() {
    var deferred = $q.defer();
    self.instance().then(function() {
      deferred.resolve(pouch);
    });
    return deferred.promise;
  }

  /*
    * Privada. Recibe un info de changes y loguea si alguno tiene conflicto.
    * 
    */ 
  function logConflictingChanges(info) {
    info.change.docs.forEach(function(doc){
      console.debug('[PersistenceService] pull change, doc: ' + JSON.stringify(doc));
      // ACÁ PUEDO AVISAR DEL CAMBIO DE CADA DOCUMENTO, PUEDO ANALIZAR SI ESTÁ BORRADO, ETC., ASÍ SE UPDATEAN LOS MODELS Y VIEWS.
      // Now check if any of these has any conflicts
      self.pouch()
        .then(function(p) {
          return p.get(doc._id, {conflicts: true});
        })
        .then(function(docu) {
          if ( docu._conflicts ) {
            console.warn('[PersistenceService] pull change _id: ' + docu._id + ' has conflicts');
          }
        })
        .catch(function(err) {
          console.error('[PersistenceService] pull change error: ' + JSON.stringify(err));
        });

    });
  }

  /*
    * Privada. Configura la synchronización
    * incluyendo la notificación de cambios cuando hay un pull:
    * cuando llegan nuevos cambios envía un 
    * $rootScope.$broadcast(PersistenceService.NEW_CHANGES, info.change.docs);
    * 
    * https://pouchdb.com/api.html#replication
    */
  function setupSync() {
    // Handoff to regular 2 way replication
    console.log('[PersistenceService] setupSync contra: ' + self.REMOTE_DB_URL);
    pouchRep = pouch.sync(self.REMOTE_DB_URL, {
      live: true,
      // filter: 'undeleted/undeletedfilter',
      retry: true
      /*
      Para replicar solamente un tipo de objeto o los que importan a este cliente, usar:
      options.filter: Reference a filter function from a design document to selectively get updates. To use a view function, pass _view here and provide a reference to the view function in options.view. See filtered replication for details.
      */
    }).on('change', function(info) {
      // This event fires when the replication has written a new document.
      // Example response ( info )
      /*
        { direction: 'pull', // o push
          change: {
            "doc_write_failures": 0,
            "docs_read": 1,
            "docs_written": 1,
            "errors": [],
            "last_seq": 1,
            "ok": true,
            "start_time": "Fri May 16 2014 18:23:12 GMT-0700 (PDT)",
            "docs": [
              { _id: 'docId',
                _rev: '1-e798a1a7eb4247b399dbfec84ca699d4',
                and: 'data' }
            ]
          }          
        }  
        */                   

      // Aviso a todas las vistas cuando hay nuevos cargados en la base
      // Las vistas deben escuchar este evento NEW_CHANGES
      if (info && info.direction === 'pull') {
        console.debug('[PersistenceService sync pull] NEW_CHANGES : '); // + JSON.stringify(info));
        if (info.change.ok) {
          // Aviso de que cambiaron docs
          $rootScope.$broadcast(PersistenceService.NEW_CHANGES, info.change.docs);
          $rootScope.$broadcast('sitesLoaded');                  
          // Si quiero procesar cada uno localmente, puedo llamar a alguna función como logConflictingChanges(info)
        } else {
          console.log('[PersistenceService sync pull] change NOT ok: ' + JSON.stringify(info));
        }
      }
    }).on('paused', function() {
      // replication paused (e.g. user went offline)*/
      console.log('[PersistenceService sync] paused: ');
    }).on('active', function() {
      // replicate resumed (e.g. user went back online)
      console.log('[PersistenceService sync] active: ');
    }).on('denied', function(info) {
      // a document failed to replicate, e.g. due to permissions
      console.log('[PersistenceService sync] denied: ' + JSON.stringify(info));
    }).on('complete', function(info) {
      // This event fires when replication is completed or cancelled. In a live replication, only cancelling the replication should trigger this event.
      console.log('[PersistenceService sync] complete: ' + JSON.stringify(info));
    }).on('error', function(err) {
      // handle error
      console.log('[PersistenceService sync] error: ' + JSON.stringify(err));
    });
    //   pouchRep.cancel(); // whenever you want to cancel
    console.log('[PersistenceService] Synchro activada');

  }


  /*
    * Carga desde el bootstrap.json ( que NO es un DUMP de PouchDB sino uno de la base de IBM )
    * 
    */ 
  function loadBootstrap() {
    // Cargo el BOOTSTRAP
    return pouch.get('bootstrap_initial_load_complete')
    .then(function (response) {
      console.log('Already bootstraped with: ' + JSON.stringify(response)); 
    })      
    .catch(function (err) {
      if (err.status !== 404) { // 404 está ok xq significa que no encontramos el flag, si es otro error es una Exception
        throw err;
      }
      $http.get(PersistenceService.BOOTSTRAP_FILE)
      .then(function(res){
        if ( res.status == 200) {
          return pouch.bulkDocs(res.data.map(function(siteData,index) {
            return Site.buildFromBootstrap(siteData);
          }));
        } else {
          console.error('status !== 200, es: ' + res.status);
        }
      })
      .then(function(result){
        console.log('Created ' + result.length  + ' docs from bootstrap');
        return pouch.put({_id: 'bootstrap_initial_load_complete'});
      })
      .then(function(result){
        console.log('bootstrap_initial_load_complete');
      })
      .catch(function(err){
        console.error('Error bootstrap_initial_load_complete: ' + JSON.stringify(err));      
      });            
    });
  }



  /*
    * Carga el dump inicial del DUMP_FILE_URL
    * 
    */ 
  function loadInitialDump() {
    // Cargo el dump inicial
    //Truco para no levantar si ya lo habíamos hecho, así no inicializa del remoto innecesariamente.
    var deferred = $q.defer();
    pouch.get('_local/initial_load_complete')
    .then(function() {
      console.log('_local/initial_load_complete already there');
      deferred.resolve();
    })
    .catch(function (err) {
      if (err.status !== 404) { // 404 está ok xq significa que no encontramos el flag, si es otro error es una Exception
        throw err;
      }
      pouch.load(self.DUMP_FILE_URL, {
        proxy: self.REMOTE_DB_URL
      })
      .then(function () {
        console.log('initial_load_complete');
        return pouch.put({_id: '_local/initial_load_complete'});
      })
       // at this point, we are sure that initial replication is complete
      .then(function () {
        console.log('_local/initial_load_complete flag saved');
        $rootScope.$broadcast('sitesLoaded');        
        deferred.resolve();
      })
      .catch(function(err){
        console.error('Error initial_load_complete: ' + JSON.stringify(err));
        deferred.reject(err);      
      });            
    });
    return deferred.promise;
 
  }

  /**
   * Inicia la sincronización con la base remota
   * Ver setupSync
   * 
   * @return self
   */
  function syncWithRemote() {
    if (!!pouchRep) {
      console.log('Ya está sincronizando');
      return self;
    }
    console.info('Activando synchro');
    getPouch()
      .then(loadInitialDump)
      .then(setupSync)
      .catch(function (err) {
        // handle unexpected errors on '_local/initial_load_complete' and on load
        console.error('[PersistenceService] Error while trying initial load: ' + JSON.stringify(err));
        setupSync();
      });
    return self;
  }

  /**
   * Detiene la sincronización con la base remota.
   *
   * Tiene que haber llamado a syncWithRemote antes, si no tira un Error
   *
   * @return self
   */
  function cancelSyncWithRemote() {
    if (pouchRep) {
      pouchRep.cancel();
    } else {
      console.warn('No existe el pouchRep');
    }
    return self;
  }
  /**
   * Resetea los datos almacenados localmente, volviendo a cargar los datos del boot file
   * Si no estaba creada la pouch no hace nada
   *
   * @return promise, se resuelve si no hay error, se rechaza si hay error
   */
  function resetLocalData() {
    var deferred = $q.defer();
    if (pouch) {
      pouch.destroy().then(function(response) {
        pouch = null;
        self.instance().then(function() {
          self.preload().then(function() {
            deferred.resolve();
          });
        });
      }).catch(function(err) {
        console.log('Error al borrar la pouchdb: ' + JSON.stringify(
          err));
        deferred.reject();
      });
    } else {
      deferred.resolve();
    }
    return deferred.promise;
  }

  /**
   * Borra los datos almacenados localmente, sin replicarlo. Usado ppalmente en los tests
   *
   * @return promise, se resuelve si no hay error, se rechaza si hay error
   */
  function deleteLocalData() {
    var deferred = $q.defer();
    self.pouch()
      .then(function(p) {
        return p.destroy();
      })
      .then(function(response) {
        pouch = null;
        deferred.resolve();
      })
      .catch(function(err) {
        console.log('Error al borrar la pouchdb: ' + JSON.stringify(err));
        deferred.reject(err);
      });
    return deferred.promise;
  }


  /**
   * Crea un nuevo doc en la base a partir del objeto
   *
   * Retorna el objecto actualizado con el nuevo id ( si fue necesario asignarle uno ) y el _rev que le asignó pouch
   */
  function create(object) {
    var defer = $q.defer();
    if (!object._id || typeof object._id === 'undefined') {
      object.newId();
    }
    self.pouch()
      .then(function(p) {
        var doc = objectToDoc(object);
        return p.put(doc);
      })
      .then(function(response) {
        if (!response.ok) {
          defer.reject(new Error(response));
        } else {
          //Actualizo el _rev
          object._rev = response.rev;
          defer.resolve(object);
        }
      })
      .catch(function(err) {
        console.log(err);
        defer.reject(err);
      });

    return defer.promise;
  }

  /**
   * Hace un update en pouch del objeto
   * "If the document already exists, you must specify its revision _rev, otherwise a conflict will occur."
   * Retorna el objecto actualizado con el nuevo _rev que le asignó pouch
   *
   * tira un Error si el objeto no tiene id y _rev
   */
  function update(object) {

    var defer = $q.defer();
    if (!object._id || typeof object._id === 'undefined' || !object._rev) {
      defer.reject(new Error(
        'El objeto no tiene id o no tiene _rev, no puedo hacer un update'
      ));
    }
    self.pouch()
      .then(function(p) {
        var doc = object;
        return p.put(doc);
      })
      .then(function(response) {
        if (!response.ok) {
          defer.reject(new Error(response));
        } else {
          //Actualizo el _rev
          object._rev = response.rev;
          defer.resolve(object);
        }
      })
      .catch(function(err) {
        console.log(err);
        defer.reject(err);
      });

    return defer.promise;
  }

  /**
   * Crea o updatea ( semántica de PUT de pouchdb http://pouchdb.com/api.html#create_document )
   *
   * "If the document already exists, you must specify its revision _rev, otherwise a conflict will occur."
   * Retorna el objecto actualizado con el nuevo _rev que le asignó pouch
   *
   */
  function createOrUpdate(object) {

    var defer = $q.defer();
    self.pouch()
      .then(function(p) {
        var doc = objectToDoc(object);
        return p.put(doc);
      })
      .then(function(response) {
        if (!response.ok) {
          defer.reject(new Error(response));
        } else {
          //Actualizo el _rev al valor del que grabé recién
          object._rev = response.rev;
          object._id = response.id;
          defer.resolve(object);
        }
      })
      .catch(function(err) {
        if (err.status === 409) {
          console.warn('A 409 error was returned, maybe you should be doing an upsert instead?');
          defer.reject(err);
        } else {
          console.log(err);
          defer.reject(err);
        }
      });

    return defer.promise;
  }

  /**
   * Es un pasamanos a db.upsert https://github.com/pouchdb/upsert
   *
   * En vez de tocar un objeto y mandar un createOrUpdate ( que puede tirar un 409),
   * en este caso pasamos el docId y una función que updatea el objeto.
   * Si el objeto no existe la función debe retornar el objeto completo
   * Si ya existe, lo updatea y lo retorna.
   * Si no quiere modificarlo, retorna false
   */
  function upsert(docId, diffFunc) {
    return self.pouch()
      .then(function(p) {
        return p.upsert(docId, diffFunc);
      });
  }

  /**
   * Borra el object de la base; tiene que tener _id y _rev
   *
   * Retorna el objecto actualizado con la _rev con la que se marcó como borrado
   */
  function remove(object) {
    var defer = $q.defer();
    if (!object._id || typeof object._id === 'undefined') {
      throw new Error(
        'el object tiene que tener un _id para poder borrarlo');
    }
    if (!object._rev || typeof object._rev === 'undefined') {
      throw new Error(
        'el object tiene que tener una _rev para poder borrarlo');
    }
    self.pouch()
      .then(function(p) {
        var doc = objectToDoc(object);
        return p.remove(doc._id, doc._rev);
      })
      .then(function(response) {
        if (!response.ok) {
          defer.reject(new Error(response));
        } else {
          //Actualizo el _rev
          object._rev = response.rev;
          defer.resolve(object);
        }
      })
      .catch(function(err) {
        console.log(err);
        defer.reject(err);
      });

    return defer.promise;
  }

  /**
   * Retorna ( un promise que se resuelve con ) los Documents cargados de la base que corresponden al tipo recibido
   *
   * tipo : String
   * @return
   *    Array de documentos encontrados
   *    throws Error cuando hay algún error al acceder a los datos
   */
  function findAllDocuments(tipo) {
    var defer = $q.defer();

    // This works because CouchDB/PouchDB _ids are sorted lexicographically.
    //'Usuario_' <= 'Usuario_(.*)' <= 'Usuario\uffff', porque '_'.charCodeAt(0) == 95 y '\uffff'.charCodeAt(0) == 65536
    self.pouch()
      .then(function(p) {
        return p.allDocs({
          include_docs: true,
          attachments: true,
          startkey: tipo + '_',
          endkey: tipo + '\uffff'
        });
      })
      .then(function(result) {
        defer.resolve(result.rows.map(function(elem) {
          return elem.doc;
        }));
      })
      .catch(function(err) {
        console.log(JSON.stringify(err));
        defer.reject(err);
      });
    return defer.promise;
  }

  /**
   * Retorna ( un promise que se resuelve con ) las instancias cargados de la base que corresponden al tipo recibido
   * Crea los objetos dinámicamente
   * FIXME Esto NO anda, es igual a findAllDocuments
   * @param tipo es el nombre de un tipo global que tiene que implementar buildFromDocument, por ejemplo Usuario
   * @return
   *    Array de objetos encontrados
   *    throws Error cuando hay algún error al acceder a los datos
   */
  function findAll(tipo) {
    var defer = $q.defer();

    // This works because CouchDB/PouchDB _ids are sorted lexicographically.
    //'Usuario_' <= 'Usuario_(.*)' <= 'Usuario\uffff', porque '_'.charCodeAt(0) == 95 y '\uffff'.charCodeAt(0) == 65536
    self.pouch()
      .then(function(p) {
        return p.allDocs({
          include_docs: true,
          attachments: true,
          startkey: tipo + '_',
          endkey: tipo + '\uffff'
        });
      })
      .then(function(result) {
        defer.resolve(result.rows.map(function(elem) {
          //FIXME Error 'buildFromDocument undefined', lo dejo comentado y devuelvo directamente element.doc
          //return (window[tipo]).buildFromDocument(elem.doc);
          return elem.doc;
        }));
      })
      .catch(function(err) {
        console.log('EROOR');
        console.log(JSON.stringify(err));
        defer.reject(err);
      });
    return defer.promise;
  }

  /**
   * Retorna ( un promise que se resuelve con ) la instancia cargado de la base que corresponde al tipo recibido
   * Crea los objetos dinámicamente usando el constructor
   *
   * @return
   *    Objetos encontrado
   *    throws Error cuando hay algún error al acceder a los datos
   */
  function findById(tipo, id) {
    var defer = $q.defer();
    if ( typeof id === 'undefined') {
      defer.reject(new Error('id es undefined'));
    } else {
      self.pouch()
        .then(function(p) {
          return p.get(id,{
          include_docs: true,
          attachments: true
          });
        })
        .then(function(doc) {
          //Como viene en undefined a veces, no puedo usar window[tipo]
          if (tipo === Site) {
            defer.resolve(Site.buildFromDocument(doc));
          } else {
            defer.reject(new Error('tipo no soportado: ' + tipo));
          }
        })
        .catch(function(err) {
          console.error('findById tipo: ' + tipo.name + ' id: ' + id + ' error: ' + JSON.stringify(err));
          defer.reject(err);
        });
    }
    return defer.promise;
  }



} //PersistenceService

// Privadas - utils

/**
 * Según http://pouchdb.com/errors.html#could_not_be_cloned ,
 * "an object with its own class (new Foo()) or with special methods like
 * getters and setters cannot be stored in PouchDB/CouchDB."
 *
 * Para estar seguros, este método retorna un document "classless" con las
 * propiedades clonadas del object
 */
function objectToDoc(object) {
  return Object.assign({}, object);
}

angular.module('agregarModal', [])

// Funcionalidad común del agregar
// Los métodos reciben como callback un controller que maneja la UI y tiene el modal

.service('agregarModalService', function($ionicModal, finderModelService, $ionicPopup){

  //Abre la ventana de Agregar sitio
  this.openModal = function(controller) {
    controller.preOpen();
    controller.agregarModal.show();
  };

  //Abre la ventana de Agregar comentario
  this.openModalComment = function(controller) {
    //controller.preOpen();
    controller.agregarModalComment.show();
  };

  //Cierra la ventana de Agregar sitio
  this.closeModal = function(controller) {
    controller.postOpen();
    controller.agregarModal.hide();
  };

  //Cierra la ventana de Agregar comentario
  this.closeModalComment = function(controller) {
    //controller.postOpen();
    controller.agregarModalComment.hide();
  };

  //Callback cuando el usuario clickea en OK, debemos agregar el site
  this.agregarSite = function(site, controller) {
    var addressFromList = false;
    for (var i = controller.validAddresses.length - 1; i >= 0; i--) {
      addressFromList = addressFromList || ( controller.validAddresses[i].streetAddress === site.direccion ); 
    };

    var self = this;
    if ( !addressFromList ) {
      controller.preOpen();
      $ionicPopup.show({
         title: 'Atención',
         template: 'La dirección no coincide con ninguna de las sugeridas, por lo que la validación llevará más tiempo. ¿Está seguro de enviarla así?',
         buttons: [
            { text: 'Ok',
              type: 'button-assertive',
              onTap: function(e) {
                return true;
              } },
            { text: 'Cancelar',
              type: 'button-assertive',
              onTap: function(e) {
                return false;
              } 
            }
            ]
       }).then(function(confirma) {
         controller.postOpen();
        if ( confirma ) {
         console.log('Confirma');
         self.primAgregarSite(site, controller);
        } else {
         console.log('No confirma');          
        }
       });
       return;          
    } else {
       self.primAgregarSite(site, controller);
    }

  }

  this.updateSite = function(site, controller) {

    controller.hideWindow();
    console.log("Actualizar fotos: " + JSON.stringify(site));
    if ( typeof site === "undefined" ) {
        console.log("site undefined. Abort");
        return;
    }

    finderModelService.addPictures(site)
      .then(function(updateSite) {
       controller.preOpen();
       $ionicPopup.show({
         title: 'Resultado',
         template: 'Imágenes actualizadas, enviadas a revisión.',
         buttons: [
            { text: 'Ok',
              type: 'button-assertive' }]
       }).then(function(res) {
         console.log('Ok');
         controller.postOpen();
       });
      })
      .catch(function(reason) {
        console.log("Error al enviar: " + JSON.stringify(reason));
        controller.preOpen();
       $ionicPopup.show({
         title: 'Resultado',
         template: 'Ocurrió un error al subir las imágenes. Por favor reintente',
         buttons: [
            { text: 'Ok',
              type: 'button-assertive' }]
       }).then(function(res) {
         console.log('Ok');
         controller.postOpen();
       });
      });

  };

  this.primAgregarSite = function(site, controller) {

    controller.agregarModal.hide();
    console.log("Agregar site: " + JSON.stringify(site));
    if ( typeof site === "undefined" ) {
        console.log("site undefined. Abort");
        return;
    }

    var location = site.position ? [site.position.lng, site.position.lat] : [];

    var siteObj = {
      "address": site.direccion,
      "location": location,
      "name": site.nombre,
      "phone": site.telefono,
      "validado": true,
      "web": site.web,
      "autor" : site.autor,
      "image" : site.image
    };

    finderModelService.addSite(siteObj)
      .then(function(newSite) {
       controller.preOpen();
       $ionicPopup.show({
         title: 'Resultado',
         template: 'El nuevo site fue enviado a revisión, una vez validado aparecerá en la lista de sitios.',
         buttons: [
            { text: 'Ok',
              type: 'button-assertive' }]
       }).then(function(res) {
         console.log('Ok');
         controller.postOpen();
       });
      })
      .catch(function(reason) {
        console.log("Error al enviar: " + JSON.stringify(reason));
        controller.preOpen();
       $ionicPopup.show({
         title: 'Resultado',
         template: 'Ocurrió un error al enviar la nueva ubicación. Por favor reintente',
         buttons: [
            { text: 'Ok',
              type: 'button-assertive' }]
       }).then(function(res) {
         console.log('Ok');
         controller.postOpen();
       });
      });

  };

  this.agregarComment = function(comment, controller) {

    controller.agregarModalComment.hide();
    console.log("Comentario agregar Modal :" + JSON.stringify(comment));
    if ( typeof comment === "undefined" ) {
        console.log("comment undefined. Abort");
        return;
    }

    finderModelService.addComment(controller.data.sites, comment)
      .then(function(newComment) {
       //controller.preOpen();
       $ionicPopup.show({
         title: 'Resultado',
         template: 'El nuevo comentario fue enviado a revisión, una vez validado aparecerá en la lista de comentarios del sitio.',
         buttons: [
            { text: 'Ok',
              type: 'button-assertive' }]
       }).then(function(res) {
         console.log('Ok');
         //controller.postOpen();
       });
      })
      .catch(function(reason) {
        console.log("Error al enviar: " + JSON.stringify(reason));
        //controller.preOpen();
       $ionicPopup.show({
         title: 'Resultado',
         template: 'Ocurrió un error al agregar el nuevo comentario. Por favor reintente',
         buttons: [
            { text: 'Ok',
              type: 'button-assertive' }]
       }).then(function(res) {
         console.log('Ok');
         //controller.postOpen();
       });
      });

  };
});




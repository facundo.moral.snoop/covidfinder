angular.module('ionicfinder')
.service('FileReaderService', function($q, $http) {

    var FileReaderService = {
      readFileAsync: readFileAsync
    }

    return FileReaderService;

    function readFileAsync(file) {
      console.log('[FileReaderService] file: '+ file);
      var deferred = $q.defer();
      $http.post(file).then(function(result) {
        console.log('[FileReaderService] in post: '+ file);
        deferred.resolve(result.data);
        console.log(result);
      }).catch(function(error) {
        deferred.reject("Error al obtener el archivo.");
      });
      // var reader = new FileReaderService();
      // reader.onloadend = function(result) {
      //   deferred.resolve(e.target.result);
      // };
      // reader.readAsText(file);


      return deferred.promise;
    }

  });

// Ionic Finder


// angular.module is a global place for creating, registering and retrieving Angular modules
// 'ionicfinder' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('ionicfinder', ['ionic', 'ionicfinder.model', 'agregarModal', 'geoLocationQueue'])

.run(function($ionicPlatform, finderModelService, PersistenceService, $window, AWS_Service, $q) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)

    //cordova plugin add ionic-plugin-keyboard --save
    if (window.cordova && window.cordova.plugins.Keyboard) {
      window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      // cordova plugin add cordova-plugin-statusbar --save
      window.StatusBar.styleDefault();
    }

    // Levantamos el acceso a la base
    PersistenceService.instance()
    .then(function(){
      console.log('apps.js: PersistenceService.instance().then');
      // return PersistenceService.loadBootstrap();
      return PersistenceService.syncWithRemote();
    })
    .catch(function(err){
      console.error('Error al arrancar PouchDB: ', err);
    });

    // HACK porque el map no es un service sino a bunch of functions
    _map_$q = $q;
    //Carga el mapa via plugin. Si estamos debuggeando en un browser no va a hacer nada
    createMap();
  });
})

.config(function($stateProvider, $urlRouterProvider, $compileProvider, $ionicConfigProvider) {


  $ionicConfigProvider.tabs.position('bottom');

  $compileProvider.imgSrcSanitizationWhitelist(/^\s*((https?|ftp|file|local):|data:image\/)/);

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    // setup an abstract state for the tabs directive
    .state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html",
      controller: 'AppCtrl'
    })

    // Each tab has its own nav history stack:

    .state('tab.mapa', {
      url: '/mapa',
      views: {
        'tab-mapa': {
          templateUrl: 'templates/tab-mapa.html',
          controller: 'MapaCtrl'
        }
      }
    })

    .state('tab.sitios', {
      url: '/sitios',
      views: {
        'tab-sitios': {
          templateUrl: 'templates/tab-sitios.html',
          controller: 'SitiosCtrl'
        }
      }
    })

    .state('tab.masInfo', {
      url: '/masInfo',
      views: {
        'tab-masInfo': {
          templateUrl: 'templates/tab-masInfo.html',
          controller: 'MasInfoCtrl'
        }
      }
    })

    .state('tab.sitiosTest', {
      url: '/sitiosTest',
      views: {
        'tab-sitios': {
          templateUrl: 'templates/tab-sitios-test.html'
        }
      }
    })

    .state('sitioDetalle', {
      url: '/sitioDetalle',
          templateUrl: 'templates/sitio-detalle.html',
          controller: 'SitiosCtrl'
    })

    ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/mapa');

});

﻿//   *************************    ESTO ESTÁ ACÁ PARA HACER COPYPASTE  *****************
// Es la versión que usa JQM y GMapsPlugin



// *** CONSTs

// var username    = "asdra-guest@kidozen.com";
// var password    = "jfkls8hjf62";

var username    = "asdra@kidozen.com";
var password    = "gebRU4hu";


//Para prod poner var application = "finder";
// En dev correr el kido local así: kido app-run finderdev asdra.kidocloud.com
var application = "finderdev";
var marketplace = "https://asdra.kidocloud.com";


//Access kidozen services through a Kido instance.
var kido = new Kido();
var logging = kido.logging();

//use this to make sure user is signed in.
var signin = $.Deferred()
    .done(function (claims) {
        // controller.loadHome();
        if (claims && claims.length) {
            for(var i in claims) {
                var c = claims[i];
                if (c &&
                    c.type  === "http://schemas.kidozen.com/role" &&
                    c.value === "Application Admin") {
                    view.showAdminControls();
                    break;
                }
            }
        }
    }).fail(function (err) {
        alert('Se produjo un error tratando de acceder al servidor.');
    });


//use this to make sure the map is created.
var mapCreated = $.Deferred();

//helper to see if it's a native app (for phonegap or cordova).
window.isNative = (document.URL.indexOf("http") !== 0);

/****************************
*
* Model: Access to the backend services and domain specific logic.
*
*****************************/
var model = {

    currentLoc : undefined,

    setCurrentLoc : function( loc) {
        model.currentLoc = loc;
    },

    authenticate: function (username, password, application, marketplace, provider) {
        kido = new Kido(application, marketplace);
        return kido.authenticate(username, password, provider).done(function (token) {
            // the user authenticated
            logging  = kido.logging();
        });
    },

    getNearbyLocations: function (coord) {
        var earthRadiusInMiles = 3959;
        var maxDistance = 2; //in miles.
        var query = {
                "location": {
                    "$nearSphere": [coord.lng, coord.lat],
                    "$maxDistance": maxDistance / earthRadiusInMiles
                }
            };
        return kido
            .storage()
            .objectSet("locations")
            .query(query);
    },

    searchAddress: function (addr) {
        return kido
            .datasources('getCoordByAddress')
            .query({addr: addr})
            .then(function (data) {
                var result = data.body.results[0];
                return result && result.geometry && result.geometry.location;
            });
    },

    saveLocation: function (data) {
        if (data._id) {
            //override the latest version
            return kido.storage().objectSet("locations").get(data._id).then(function (old) {
                data._metadata = old._metadata;
                return kido.storage().objectSet("locations").update(data);
            });
        } else {
            return kido.storage().objectSet("locations").insert(data);
        }
    },

    getLocations: function () {
        return kido.storage().objectSet("locations").query();
    },

    getLocation: function (id) {
        return kido.storage().objectSet("locations").get(id);
    }
};

/****************************
*
* Controller: in charge of calling the model and rendering the right view.
*
*****************************/
var controller = {

    signin: function () {
        model
            .authenticate(username, password, application, marketplace, "Kidozen")
            .done(function (token) {
                signin.resolve(token.claims);
            })
            .fail(function (err) {
                // alert('Pinchó el signin.');
                signin.reject(err);
            });
    },

    loadHome: function () {
        //Fs para maps plugin
        var onSuccess = function(location) {
            console.log('ubicación actual: ' + JSON.stringify(location));
            var currentLoc = {
                lat: location.latLng.lat,
                lng: location.latLng.lng
            };
            model.setCurrentLoc(currentLoc);
            view.updateMap(location);
            signin
                .then(function () {
                    return model.getNearbyLocations(currentLoc);
                })
                .done(view.showHomeLocations)
                .always(view.hideLoading);
        };

        var onError = function(err) {
            console.log('No pude obtener la ubicación actual: ' + JSON.stringify(err));
            view.hideLoading();
        };

        // En iOS, si no pido la currentPosition una vez que está deviceready sale un alert horrible pidiendo permisos al usuario
        // http://stackoverflow.com/questions/1673579/location-permission-alert-on-iphone-with-phonegap
        var onDeviceReady = function () {
            console.log('onDeviceReady de loadHome');
            mapCreated.
                then(function() {
                    view.showLoading();
                    map.getMyLocation(onSuccess, onError);
                });
        };

        if (window.isNative) {
            document.addEventListener("deviceready", onDeviceReady, false);
        } else {
            onDeviceReady();
        }

        signin
                .then(function () {
                    mapCreated.then(function () {
                        model
                            .getLocations()
                            .done(view.addLocationsToMap)
                            .fail(function (err) {
                                console.log('loadLocations error: ' + JSON.stringify(err));
                            });
                    });
                });

    },

    loadNewLocation: function () {
        view.showEditPage(null);
    },
    
    searchAddress: function () {
        if ( view.edit.address() && view.edit.address() != '' ) {
            view.withLoading(
                model
                    .searchAddress(view.edit.address())
                    .done(view.edit.showLocation)
                );
        } else {
            //TODO cambiar lógica para que el botón buscar esté deshabilitado hasta que ingrese algo
            console.log('buscar es vacío');
        }

    },

    saveAddress: function () {
        var id = view.edit.id();
        var address = view.edit.address();
        var name = view.edit.name();
        var phone = view.edit.phone();
        var web = view.edit.web();
        var loc = view.edit.newLoc;

        if (!address || !name) {
            alert("Por favor complete la dirección y el nombre.");
            return;
        }

        view.withLoading(
            model.saveLocation({
                _id: id,
                name: name,
                address: address,
                phone: phone,
                web: web,
                location: loc
            }).then(function () {
                controller.loadLocations();
            })
        );
    },

    loadLocations: function () {
        view.withLoading(
            signin.then(function () {
                return model
                    .getLocations()
                    .done(view.showLocations)
                    .fail(function (err) {
                        console.log('loadLocations error: ' + JSON.stringify(err));
                    });
            })
        );
    },

    loadLocation: function (id) {
        view.showLocationDetailsPage();
        view.withLoading(
            model.getLocation(id).done(view.showLocationDetails)
        );
    },

    editLocation: function (id) {
        console.log("controller editLocation");
        view.withLoading(
            model.getLocation(id).done(view.showEditPage).done(controller.searchAddress)
        );
    }
};


/****************************
*
* View: render the information in the HTML though dinamic DOM manipulation.
*
*****************************/
var view = {

    infowindow : undefined,

    updateMap: function (location) {

        console.log('updateMap location: ' + JSON.stringify(location));
        // map.animateCamera({
        //   target: new plugin.google.maps.LatLng(location.latLng.lat, location.latLng.lng),
        //   zoom: 14
        //   ,'duration': 10000
        // }, function() {
        //   console.log("The animation is done.");
        // });


        mapCreated.then(function () {
            console.log("updateMap then block");
            // map.moveCamera({
            map.animateCamera({
              target: new plugin.google.maps.LatLng(location.latLng.lat, location.latLng.lng),
              zoom: 14,
              duration: 5000
            }, function() {
              console.log("The animation is done.");
            });
        });
    },

    showEditPage: function (item) {
        console.log('view showEditPage');
        if (item) {
            //it's an edit.
            $("#edit-id").val(item._id);
            $("#edit-name").val(item.name);
            $("#edit-address").val(item.address);
            $("#edit-web").val(item.web);
            $("#edit-phone").val(item.phone);
        } else {
            //it's a new
            $("#edit-location input").val('');

            view.edit.showLocation(model.currentLoc);
        }
        // $.mobile.changePage('#edit-location');
    },

    edit: {

        marker : undefined ,

        newLoc : undefined,

        id: function () {
            return $("#edit-id").val();
        },
        name: function () {
            return $("#edit-name").val();
        },
        address: function () {
            return $('#edit-address').val();
        },
        phone: function () {
            return $("#edit-phone").val();
        },
        web: function () {
            return $("#edit-web").val();
        },

        setNewLocFromCoord : function ( coord ) {
            if ( coord ) {
                view.edit.newLoc = [coord.lng, coord.lat];

                var request = {
                  'position': coord
                };
                plugin.google.maps.Geocoder.geocode(request, function(results) {
                    console.log("Reverse geocode results: " + JSON.stringify(results));
                  if (results.length) {
                    var result = results[0];
                    var address = [
                      result.thoroughfare || "",
                      result.subThoroughfare || "",
                      result.locality || "",
                      result.subAdminArea || "",
                      result.adminArea || "",
                      // result.postalCode || "",
                      result.country || ""].join(", ");

                      //Si viene el detalle en iOS, tomo esto que es una mejor descripción
                      if ( result.extra && result.extra.lines ) {
                        address = result.extra.lines.join(", ");
                      }
                    $('#edit-address').val(address);
                  } else {
                    console.log("Not found");
                  }
                });
                /* Ejemplo de resultado:
                android:
                {"position":{
                    "lng":-58.1087148,
                    "lat":-34.8603423}
                ,"adminArea":"Buenos Aires Province",
                "extra":{"featureName":"2100-2198"},
                "locale":"en_US",
                "countryCode":"AR",
                "locality":"Villa Elisa",
                "thoroughfare":"Calle 24",
                "subThoroughfare":"2100-2198",
                "subAdminArea":"La Plata",
                "country":"Argentina"},

                iOS
                {"subAdminArea":"",
                "extra":{"lines":["Doctor Tomás Manuel de Anchorena 601-649","Buenos Aires, Autonomous City of Buenos Aires"]},
                "position":{"lat":-34.60221862792969,"lng":-58.40943908691406},
                "adminArea":"(null)",
                "subLocality":"Balvanera",
                "countryCode":"AR",
                "thoroughfare":"601-649 Doctor Tomás Manuel de Anchorena",
                "locale":"",
                "lat":-34.60221862792969,
                "subThoroughfare":"",
                "postalCode":"(null)",
                "lng":-58.40943908691406,
                "country":"Argentina",
                "locality":"Buenos Aires"}

                */


            }
        },

        setNewLocFromPosition: function ( latLng ) {
            view.edit.newLoc = [latLng.lng(), latLng.lat()];
        },

        showLocation: function (coord) {
            view.edit.setNewLocFromCoord(coord) ;

            if ( view.edit.marker ) {
                view.edit.marker.setPosition(coord);
            } else {
                map.addMarker({
                  'position': coord,
                  'draggable': true,
                  'title': 'Nueva ubicación',
                  // 'snippet': 'A: ' + (loc.address === undefined ? '' : loc.address ) + 
                  //   ' T: ' + (loc.phone === undefined ? '' : loc.phone ) + 
                  //   ' W: ' + (loc.web === undefined ? '' : loc.web ),
                  'icon' : 'www/img/ic-papel.png',  // Pongo uno distinto para que se distinga del resto
                  'markerClick': function(marker) {
                    console.log("Marker is clicked");
                  },
                  'infoClick': function(marker) {
                    console.log("InfoWindow is clicked");
                    // marker.remove();
                  }
                }, function(marker) {
                  view.edit.marker = marker;
                  marker.addEventListener(plugin.google.maps.event.MARKER_DRAG_END, function(marker) {
                    console.log('before marker.getPosition: ' + marker);
                    marker.getPosition(function(latLng) {
                      console.log('in marker.getPosition: ' + JSON.stringify(latLng));
                      view.edit.marker.setTitle(latLng.toUrlValue());
                      view.edit.marker.showInfoWindow();
                      view.edit.setNewLocFromCoord(latLng);
                    });
                  });
                });
            } 

        }
    },

    addLocationsToMap: function ( locs ) {
        for(var i in locs) {
            var loc = locs[i];

            console.log('loc: ' + JSON.stringify(loc));
            //add marker to map

            if ( typeof plugin !== "undefined" ) {
                var markerPos = new plugin.google.maps.LatLng(loc.location[1],loc.location[0]);
                var markerOpts = {
                  position: markerPos,
                  title: loc.name,
                  snippet: 'A: ' + (typeof loc.address === "undefined" ? '' : loc.address ) + 
                    ' T: ' + (typeof loc.phone === "undefined" ? '' : loc.phone ) + 
                    ' W: ' + (typeof loc.web === "undefined" ? '' : loc.web ),
                  markerClick: function(marker) {
                    console.log("Marker is clicked");
                  },
                  infoClick: function(marker) {
                    console.log("InfoWindow is clicked");
                    // marker.remove();
                  }
                };
                // console.log("markerOpts: " + JSON.stringify(markerOpts));
                // console.log("map: " + JSON.stringify(map));
                map.addMarker(markerOpts, function(marker) {
                  console.log("marker created");
                });

            }
        }
    },


    //Plugin marker: https://github.com/wf9a5m75/phonegap-googlemaps-plugin/wiki/Marker
    //Para armar un infoWindow a mano ( multitext, imagenes etc ) ver
    //https://github.com/wf9a5m75/phonegap-googlemaps-plugin/wiki/Marker#base64-encoded-icon
    showHomeLocations: function (locs) {
        console.log('showHomeLocations init');
        view._showLocations("#home-locations", locs);
    },

    showLocations: function (locs) {
        $.mobile.changePage("#locations");
        view._showLocations("#locations-list", locs);
    },

    _showLocations: function (id, locs) {
        var $list = $(id).html('');
        if (!locs || !locs.length) {
            $list.html('<li>No se encontraron sitios cercanos.</li>');
        } else {
            for(var i in locs) {
                var loc = locs[i];
                //add item to list
                var li =
                    '<li>' +
                        '<a href="#" data-id="' + loc._id + '">' +
                            loc.name +
                        '</a>' +
                    '</li>';
                $list.append(li);
            }
        }
        $list.listview("refresh").trigger("create");
    },

    showLocationDetailsPage: function () {
        //clean the fields, and load the page.
        $("#location-details detail").html('');
        $.mobile.changePage("#location-details");
    },

    //FIXME
    showLocationDetails: function (loc) {
        //set the map details.
        // var map = $("#location-map")[0].map;
        // var coord;
        // if ( loc.location.B && loc.location.k ) {
        //     coord = {lat: loc.location.k, lng: loc.location.B};
        // } else {
        //     coord = {lat: loc.location[1], lng: loc.location[0]};
        // }


        // map.setCenter(coord);
        // var marker = new google.maps.Marker({
        //     map: map,
        //     position: coord
        // });

        //set the rest of the fields.
        $("#location-id").val(loc._id);
        $("#location-name").html(loc.name);
        //sanitize the urls
        if (!loc.web)
            $("#location-web").html('');
        else {
            if (loc.web.indexOf('http://') !== 0 && loc.web.indexOf('https://') !== 0)
                loc.web = 'http://' + loc.web;
            var href = "javascript:window.open('" + loc.web + "', '_system', 'location=yes');";
            $("#location-web").html('<a href="' + href + '">' + loc.web + '</a>');
        }
        $("#location-address").html(loc.address);
        $("#location-phone").html(loc.phone);
    },

    showLoading: function () {
        setTimeout(function () { // hack to run inside of pagecreate
            $.mobile.loading('show');
        }, 1);
    },
    hideLoading: function () {
        $.mobile.loading('hide');
    },
    withLoading : function (promise) {
        if (promise.state() === 'pending') {
            view.showLoading();
            promise.always(view.hideLoading);
        }
    },
    showAdminControls: function () {
        $("#location-edit").show();
        $("#new-location").show();
    }
};

/************************
*
* Event Handlers: here we set up the listeners for events, gather the
* information from the DOM, and call the right controller.
*
*************************/
if (window.isNative) {
    // document.addEventListener('deviceready', debugDelay, false);    
    document.addEventListener('deviceready', createMaps, false);
    // as soon as the device is ready, sign in.
    document.addEventListener('deviceready', controller.signin, false);
    controller.loadHome();

} else {
    // if it's hosted in KidoZen or is in development mode
    // there's no need for sign in.
    kido.security().getLoggedInUser()
        .done(function (claims) {
            signin.resolve(claims);
        }).fail(function (err) {
            //do not fail, resolve with non-admin user.
            signin.resolve([]);
        });
    createMaps();
    controller.loadHome();
}

function onMapInit(map) {
    console.log('map ready');
}

//El Singleton de maps plugin
var map = undefined;

function debugDelay() {
    setTimeout(createMaps, 5000);
}
function createMaps() {

    if ( typeof plugin === "undefined" ) {
        return;
    }
    console.log('Creating maps');
    var mapDiv = document.getElementById("home-map");
    const SNOOP_BSAS = new plugin.google.maps.LatLng(-34,602219,-58,409438);
    const VILLA_ELISA = new plugin.google.maps.LatLng(-34.8605537,-58.1084798);
    // Initialize the map plugin
    map = plugin.google.maps.Map.getMap(mapDiv,{
      'backgroundColor': 'white',
      'mapType': plugin.google.maps.MapTypeId.ROADMAP,   //HYBRID
      'controls': {
        'compass': true,
        'myLocationButton': true,
        'indoorPicker': true,
        'zoom': true  // Only for Android
      },
      'gestures': {
        'scroll': true,
        'tilt': true,
        'rotate': true,
        'zoom' : true
      }
      ,
      'camera': {
        'latLng': VILLA_ELISA,
        // 'tilt': 30,
        'zoom': 1,
        'bearing': 00
      }
    });

    map.on(plugin.google.maps.event.MAP_READY, onMapInit);

    map.on(plugin.google.maps.event.CAMERA_CHANGE, onMapCameraChanged);

    mapCreated.resolve(map);
}

function onMapCameraChanged(position) {
  console.log(JSON.stringify(position));
  //FIXME
  //model.saveCurerntLoc(position) o algo así

}


$( document ).delegate("#page1", "pageinit", function () {
    $("#home-locations").delegate("li a", "click", function () {
        var id = $(this).attr('data-id');
        controller.loadLocation(id);
    });
});

$( document ).delegate( "#page1", "pageshow", function () {
    console.log('page1 pageshow');
});

$( "#menu" ).on( "panelcreate", function( event, ui ) {
    console.log('menu created');
    $("#new-location-menu").click(function () {
        controller.loadNewLocation();
    });

} );


$( document ).delegate( "#locations", "pageinit", function () {
    controller.loadLocations();
    $("#new-location").click(function () {
        controller.loadNewLocation();
    });
    $("#locations-list").delegate("li a", "click", function () {
        var id = $(this).attr('data-id');
        controller.loadLocation(id);
    });
});

$( document ).delegate( "#edit-location", "pageinit", function () {
    $("#edit-search").click(controller.searchAddress);
    $("#edit-save").click(controller.saveAddress);
});

$( document ).delegate( "#edit-location", "pageshow", function () {
    console.log('edit-location pageshow');
    // document.getElementById('edit-map').appendChild(document.getElementById('home-map'));
    // map.setVisible(true);

    //otra ( tampoco anda )
    // var mapDiv = document.getElementById("edit-map");
    // map.setDiv(mapDiv);
    // map.refreshLayout();
    // map.setVisible(true);
});

$( document ).delegate( "#location-details", "pageinit", function () {
    $("#location-edit").click(function () {
        var id = $("#location-id").val();
        controller.editLocation(id);
    });
});


$('#agregar-collapsible').on( "collapsibleexpand", function( event, ui ) {
    console.log('Expanded');
    $("#home-map").css("height", 0);
    controller.loadNewLocation();
}).on( "collapsiblecollapse", function( event, ui ) {
    console.log('Collapsed');
    $("#home-map").css({height: '300px' });
});


angular.module('ionicfinder')
.controller('SitiosCtrl', function ($scope,$ionicModal, finderModelService, agregarModalService, $ionicScrollDelegate, geoLocationQueueService, $ionicPlatform, $rootScope, $ionicLoading, AWS_Service, $q) {
  $scope.data = { "sites" : [], "sitesToShow" : [] ,"search" : '', "site": '', "settingsList": { "activeBarrio": '', "barrio": 'Barrio Norte', "activeCategoria": '', "categoria": 'Abogado', "orderAsc": '', "orderDesc": 'Descendente' } };

  //Modal de agregar sitio
  //Carga el template del model AgregarSitio
  $ionicModal.fromTemplateUrl('templates/add-site.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.agregarModal = modal;
  });

  $ionicModal.fromTemplateUrl('templates/add-comment.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.agregarModalComment = modal;
  });

  $scope.cameraOnTap = function(id, isCameraBtn) {
    console.log(id);
    console.log('En cameraOnTap');
    if (angular.isUndefined(navigator.camera)) {
      if (id != undefined) {
        var fileElem = document.getElementById('take-picture-'+id);
      } else {
        var fileElem = document.getElementById('take-picture');
      }
      if (fileElem) {
        fileElem.click();
        //El click va a hacer que el usuario elija una foto y se resuelve en el onChange handler, así que acá terminamos
      } else {
        throw new Error(
            'navigator.camera es undefined (corriendo en un browser?) y fileElem tambien ( desapareció el input file??)'
        );
      }
    } else {
        // See http://docs.phonegap.com/en/edge/cordova_camera_camera.md.html#camera.getPicture
        // Puedo usar DATA_URL y ya vuelve el base64
        var options = {
            quality: 100,
            destinationType: Camera.DestinationType.FILE_URL,
            sourceType: (isCameraBtn) ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            encodingType: Camera.EncodingType.PNG,
            correctOrientation: false,
            targetWidth: 200,
            targetHeight: 200
        };
        navigator.camera.getPicture(function(imageURI) {
          // The image is returned as a base64 encoded String or as the URI of an image file. See options above
          if (imageURI) {
            // Le pedimos que sea un FILE_URL
            console.log('[controller] imageURI: ' + imageURI);
            //cordova-plugin-file
            window.resolveLocalFileSystemURL(imageURI, function (fileEntry) {
              fileEntry.file(function (file) {
                console.log('[controller] imageFile: ' + file);
                $scope.previewImage(imageURI);
                if ($scope.obj_files == undefined)
                  $scope.obj_files = [];
                $scope.obj_files[$scope.obj_files.length] = file;
              });
            });
          }
        }, function(err) {
            throw err;
        }, options);
    }
  };

  $scope.previewImage = function(imgURL) {
    var img = new Image(); // Create new img element
    img.addEventListener("load", function() {
      // Dibujo la imagen para obtener el base64 del canvas
      var canvas = document.createElement('canvas');
      canvas.width = img.width;
      canvas.height = img.height;
      var context = canvas.getContext('2d');
      context.drawImage(img, 0, 0);
      var imageDataURI = canvas.toDataURL('image/png');
      console.info('PNG imageDataURI.length = ' + imageDataURI.length);

      var jpegDataURI = canvas.toDataURL("image/jpeg", 0.5);
      console.info('JPEG imageDataURI.length = ' + jpegDataURI.length);

      if ($scope.imageDataURI == undefined)
        $scope.imageDataURI = [];

      $scope.$apply(function() {
        $scope.imageDataURI[$scope.imageDataURI.length] = jpegDataURI;
      })
    }, false);
    // Set img src to ObjectURL
    img.src = imgURL;
  };

  //PARCHE para poder levantar imagenes cuando corremos en un browser
  var hookUpFileHandler = function(id) {
    if (id != undefined) {
      var takePicture = document.querySelector('#take-picture-'+id);
    } else {
      var takePicture = document.querySelector('#take-picture');
    }

    if (takePicture) {
      // Set events
      takePicture.onchange = function(event) {
        // Get a reference to the taken picture or chosen file
        var files = event.target.files, file;
        if (files && files.length > 0) {
          file = files[0];
          var URL = window.URL || window.webkitURL;
          var imgURL = URL.createObjectURL(file);
          $scope.previewImage(imgURL);
          if ($scope.obj_files == undefined)
            $scope.obj_files = [];
          $scope.obj_files[$scope.obj_files.length] = file;
        }
      };
    } else {
      console.error('No pude encontrar el #take-picture')
    }
  };

  //Callback al hacer tap en el botón "Agregar"
  $scope.agregarTap = function() {
    agregarModalService.openModal($scope);
    hookUpFileHandler();
    var img = AWS_Service.getImage();
    $scope.limpiarFormAgregar();
  };

  $scope.limpiarFormAgregar = function() {
    document.querySelector('.agregarForm').reset();
    $scope.data.site = '';
    $scope.clearPictures();
  }

  $scope.agregarTapComentario = function(id_site) {
    $scope.id_site = id_site;
    agregarModalService.openModalComment($scope);
    document.getElementsByClassName('name-comment')[0].value = '';
    document.getElementsByClassName('message-comment')[0].value = '';
  };

  //Callback cuando el usuario cancela el add
  $scope.agregarCancelled = function() {
    $scope.limpiarFormAgregar();
    agregarModalService.closeModal($scope);
  };

  $scope.agregarCancelledComment = function() {
    agregarModalService.closeModalComment($scope);
  };

  //Callback cuando el usuario clickea en OK, debemos agregar el site
  $scope.agregarSite = function() {
    var site = $scope.data.site;
    showLoading();
    
    Promise.all(
      $scope.obj_files.map(function(file){
        //console.log('##############333333 '+file+' ######'+$scope.obj_files+' #####'+key+' #####'+site.image);
        var defer = $q.defer();

        file.name = Math.floor(Math.random() * 100000000000000000000)+'.jpg';

        AWS_Service.uploadFile(file)
        .then( function(data){
          if ((site.image == undefined) || (site.image == '')) {
            site.image = data;
          } else {
            site.image = site.image+'#image#'+data;
          }
          defer.resolve(site.image);
        },function (err) {
          site.image = '';
          defer.reject(site.image);
          console.log('Error: El sitio no pudo subirse a Amazon');
        });

        return defer.promise;
      })
    ).then(function(){
      console.log('despues: '+JSON.stringify(site));
      agregarModalService.agregarSite(site, $scope);
      $scope.limpiarFormAgregar();
      $scope.clearPictures();
      hideLoading();
    });
  };

  $scope.agregarPictures = function(site) {
    var site = $scope.data.site;
    showLoading();

    Promise.all(
      $scope.obj_files.map(function(file){
        //console.log('##############333333 '+file+' ######'+$scope.obj_files+' #####'+key+' #####'+site.image);
        var defer = $q.defer();
        
        file.name = Math.floor(Math.random() * 100000000000000000000)+'.jpg';

        AWS_Service.uploadFile(file)
        .then( function(data){
          if ((site.image == undefined) || (site.image == '')) {
            site.image = data;
          } else {
            site.image = site.image+'#image#'+data;
          }
          defer.resolve(site.image);
        },function (err) {
          site.image = '';
          defer.reject(site.image);
          console.log('Error: El sitio no pudo subirse a Amazon');
        });

        return defer.promise;
      })
    ).then(function(){
      console.log('despues: '+JSON.stringify(site));
      agregarModalService.updateSite(site, $scope);
      $scope.limpiarFormAgregar();
      $scope.clearPictures();
      hideLoading();
    });
  };

  $scope.agregarComment = function(comment) {
    comment.id_site = $scope.id_site;
    delete $scope.id_site;
    console.log('Comentario controller: '+JSON.stringify(comment));
    agregarModalService.agregarComment(comment, $scope);
  };

  $scope.preOpen = function() {
  };

  $scope.postOpen = function() {
  };

  $scope.$on('$destroy', function() {
    $scope.agregarModal.remove();
  });

//FIN modal agregar sitio

////////////////////////////////////////////

//Modal de Filtros
  //Carga el template del modal Filtros
    $ionicModal.fromTemplateUrl('templates/filtro.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.filtroModal = modal;
    });

    $scope.reorderSites = function() {
      showLoading();

      finderModelService.loadSites()
      .then( function(data){
        if ( (document.getElementsByClassName('orderAsc')[0].firstChild.checked) && (data[0].hearts > data[data.length-1].hearts) )
          data = data.reverse();

        if ( (document.getElementsByClassName('orderDesc')[0].firstChild.checked) && (data[0].hearts < data[data.length-1].hearts) )
          data = data.reverse();

        $scope.data.sites = data;
        $scope.data.sitesToShow = data;
        $scope.data.sitesLength = $scope.data.sitesToShow.length;

        setTimeout(function(){
          $scope.filtrarCancelled();
          hideLoading();
        }, 500);
      },function (err) {
        console.log('[SitiosCtrl] Error en carga de sites: ' + JSON.stringify(err, 0, 2));
      });
    }

    //Callback al hacer tap en el botón "Filtrar"
    $scope.filtrarTap = function() {
      $scope.filtroModal.show();
    };

    $scope.filtrarCancelled = function() {
    $scope.filtroModal.hide();
  };

    $scope.$on('$destroy', function() {
    $scope.filtroModal.remove();
  });
//FIN modal filtros sitio


//////////////////////////////////////////////


  $ionicModal.fromTemplateUrl('templates/view-site.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.siteView = modal;
  });

  $scope.showSite = function(){
    $scope.openSiteView();
  }

  $scope.search = function() {
    var sites = finderModelService.loadSites().$$state['value'];
    var matches = sites.filter( function(site) {
      if(site.name.toLowerCase().indexOf($scope.data.search.toLowerCase()) !== -1 ) return true;
    })
    $ionicScrollDelegate.scrollTop();
    $scope.data.sitesToShow = matches;
  }

  $scope.setSite = function(site){
    $scope.data.search = site.name;
    $scope.data.site = site;
    $scope.data.sitesToShow = [];
  }

  $scope.setHearts = function(site, cant){
    finderModelService.addHearts(site, cant);
    showLoading();
    setTimeout(function(){
      hideLoading();
    }, 500);
  }

  $scope.openSiteView = function(){
    $scope.siteView.show();
    setTimeout(function(){
      hookUpFileHandler($scope.data.site._id);
    }, 1000);
  }

  $scope.hideWindow = function(){
    $scope.data.search = '';
    $scope.data.site = '';
    $scope.data.sitesToShow = $scope.data.sites;
    $scope.siteView.hide();
    $scope.clearPictures();
  };

  $scope.clearPictures = function() {
    $scope.obj_files = [];
    angular.forEach(document.querySelectorAll('.agregarForm .preview-img img'), function(img, key) {
      img.parentNode.remove();
    });
    return;
  }

  $scope.cancelSearch = function(){
    $scope.data.search = '';
    $scope.data.site = '';
    $scope.data.sitesToShow = $scope.data.sites;
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.siteView.remove();
  });

  geoLocationQueueService.init( function(normalizedAddrs) {
    console.log("normalizedAddrs:  " + JSON.stringify(normalizedAddrs));
    $scope.addresses = normalizedAddrs;
    $scope.validAddresses = normalizedAddrs;
    $scope.$apply();
  });

  $scope.searchAddress = function (address) {
    if ( typeof address === "undefined") {
      console.log("address es undefined");
    }

    if ( !address || (address && ( address.length < 5 || address.indexOf(" ") === -1 ))) {
      //no parece una dirección completa
      $scope.addresses = [];
      $scope.validAddresses = [];
      return;
    }

    console.log("searchAddress");
    geoLocationQueueService.push(address);
  }

  $scope.addressSelected = function (a) {
    console.log("addressSelected: " + JSON.stringify(a));
    $scope.addresses = [];
  }

  $scope.reload = function(){
    loadPartial(0);
  }

  $scope.loadMoreData = function(){
    console.log("[SitiosCtrl] length de datos:" + $scope.data.sites.length);
    if ( $scope.data.sites && $scope.data.sites.length > 0){
      loadPartial($scope.data.sites.length);
    } else {
      loadPartial(0);
    }
    return;
  }

  var loadPartial = function(index){
    finderModelService.loadPartial(index)
    .then( function(data) {
      if ( index == 0) {
        $scope.data.sites = data;
        $scope.data.sitesToShow = data;
      } else {
        $scope.data.sites = $scope.data.sites.concat(data);
        $scope.data.sitesToShow = $scope.data.sitesToShow.concat(data);
      }
      console.log("DATA RESULT length:" + $scope.data.sites.length);
      $scope.data.sitesLength = $scope.data.sitesToShow.length;
      if ( $scope.data.sitesLength > 0) {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    })
    .catch ( function (err) {
      console.err(err);
    })
  }

  $scope.onEnterView = function() {
    console.log('[SitiosCtrl] load');
    $scope.loadMoreData();
  }

  $rootScope.$on('refreshSites', function(){
    console.log('[SitiosCtrl] refreshSites handler');
    $scope.reload();
  });

  showLoading = function() {
    $ionicLoading.show({
      template: 'Cargando...',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 300,
      showDelay: 0
    });
    }

    hideLoading = function() {
      $ionicLoading.hide();
  }

});

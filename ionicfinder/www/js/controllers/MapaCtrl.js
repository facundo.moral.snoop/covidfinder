angular.module('ionicfinder')
.controller('MapaCtrl', function ($rootScope, $scope, $ionicModal, $ionicPopup, $ionicActionSheet, $timeout, finderModelService, agregarModalService, $ionicPlatform) {

  //Modal de agregar sitio
    //Carga el template del model AgregarSitio
    $ionicModal.fromTemplateUrl('templates/add-site.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.agregarModal = modal;
    });

    //Callback al hacer tap en el botón "Agregar"
    $scope.agregarTap = function() {
      agregarModalService.openModal($scope);
    };

    //Callback cuando el usuario cancela el add
    $scope.agregarCancelled = function() {
      agregarModalService.closeModal($scope);
    };

    //Callback cuando el usuario clickea en OK, debemos agregar el site
    $scope.agregarSite = function(site) {
      agregarModalService.agregarSite(site, $scope);
    };

    //Para acomodar el mapa antes de abrir un popup, popover u otra cosa que aparezca sobre el mapa
    $scope.preOpen = function() {
      if ( typeof map !== "undefined" ) {
        map.setClickable(false);
      } else {
        console.log("map undefined - probablemente dev");
      }
    };

    //Para acomdar el mapa después de abrir un popup, popover u otra cosa que aparezca sobre el mapa
    $scope.postOpen = function() {
      if ( typeof map !== "undefined" ) {
        map.setClickable(true);
      } else {
        console.log("map undefined - probablemente dev");
      }
    };

    $scope.$on('$destroy', function() {
      $scope.agregarModal.remove();
    });

  //FIN modal agregar sitio

    //Abre el actionSheet con las acciones para un marker
    $scope.openActionSheet = function (marker) {

      if ( typeof marker === "undefined" || !marker ) {
        console.log("Marker undefined?");
        return;
      }
      $scope.preOpen();

      var siteDTO = marker.get("site");
      var site;
      if ( !siteDTO ) {
        console.log("Sin el site no puedo hacer nada");
        $scope.postOpen();
        return;
      } else {
        site = new Site(siteDTO);
        console.log('ActionSheet para site:' + JSON.stringify(site));
      }

      var buttons = [];
      var actions = [];
      if ( site.hasWeb()) {
        buttons.push({ text: 'Ir a la página web' });
        actions.push(function(){site.browse();});
      }
      if ( site.hasLocation()) {
        buttons.push({ text: 'Mostrar como llegar' });
        actions.push(function(){site.navigate();});
      }
      if ( site.hasPhone()) {
        buttons.push({ text: 'Llamar por teléfono' });
        actions.push(function(){site.call();});
      }
      //Siempre podemos compartir ( si cargué el plugin ).
      if ( site.canShare()) {
        buttons.push({ text: 'Compartir' });
        actions.push(function(){site.share();});
      }

      $ionicActionSheet.show({
       buttons: buttons,
       // titleText: 'Acciones con el sitio',
       cancelText: 'Cancelar',
       cancel: function() {
          $scope.postOpen();
        },
       buttonClicked: function(index) {
          $scope.postOpen();
          var action = actions[index];
          action();
          return true;
       }
     });
      //Esta línea mágica es necesaria para que efectivamente aparezca el actionsheet, sino queda escondido ( supongo q debajo del mapa)
      // Ver http://forum.ionicframework.com/t/ionicactionsheet-appears-to-be-hidden-by-ionicbackdrop-starting-in-beta-v8/6517/4
      $timeout(function() {
        $scope.$apply();
      },1);
    };

    $scope.reload = function(){
      finderModelService.loadSites()
      .then( function(data){
        console.log("[mapaCtrl] SITES TO SHOW length: "+JSON.stringify(data.length));
  	    addSitesToMap(data, $scope);
      },function (err) {
        console.log('[mapaCtrl] Error en carga de sites: ' + JSON.stringify(err, 0, 2));
      });
    }

    //Init del controller
    //Carga los sites y los agrega al mapa como markers
    $ionicPlatform.ready(function() {
      console.log('MapaCtrl $ionicPlatform.ready reload');
      $scope.reload();
    });

    $rootScope.$on('refreshSites', function(){
      console.log('MapaCtrl refreshSites handler');
      $scope.reload();
    });

  //Fin MapasCtrl
});

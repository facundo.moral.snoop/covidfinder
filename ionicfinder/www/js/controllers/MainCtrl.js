angular.module('ionicfinder')
.controller('MainCtrl', function ($scope) {
  $scope.refreshMap = function() {
    setTimeout(function () {
      $scope.refreshMapReally();
    }, 1);
  };

  $scope.refreshMapReally = function() {
   var div = document.getElementById("map_canvas");

   //FIXME colgarlo de un deferred que se resuelva cuando se cree un mapa
   if ( typeof map !== "undefined" ) {
      console.log("map still alive");
      //	  	map.reattachTo(div);
      reattachMap(map,div);
      fixMapHeight();
      console.log("map reattached");
    } else {
      console.log("map undefined");
    }
 };

});

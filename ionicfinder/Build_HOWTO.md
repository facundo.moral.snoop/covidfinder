# Build de un release

## Manejo de branches

Trabajamos sobre el master, y los cambios pasan a prod_env y a prod_env_android:

Diferencias entre master y prod_env

- Base de datos: en `master` apunta a la dev, y en `prod_env` a la prod. Ver `services/env.js` y `PersistenceService.js`. En el branch `prod_env`, el `env.js` tiene los datos de prod, y en el `master` no tiene nada porque toma el default de `PersistenceService.js`
- Dump de la base: en master usamos uno de la base "dev", y en prod_env de prod

**NUNCA** hacemos un merge desde prod_env a master, siempre se hacen los cambios en master, se mergean a prod_env y de ahí se publica.

## Dumps de la base

Cada vez que hacemos un release es recomendable tomar un dump actualizado de la base de prod, así la versión en el store tiene una base reciente cuando arranca y no tiene que hacer tanta sincronización.

**OJO** esto en el branch `prod_env`

( Instalar npm install -g pouchdb-dump-cli si no está )


`pouchdb-dump http://admin:TlccaS.1986@asdra_prod.snoopconsulting.com:5984/asdra-prod > www/dbdumps/asdra-prod-azure.dump`

Y hacer commit en el branch de prod_env.


## Preparación del build

En el branch `master`

1) Updatear la versión en config.xml
6.0.1

setear:

2) export ASDRA_VERSION=6.0.1

4) git tag v_$ASDRA_VERSION

5) git push --tags

6) Probar que anda OK para Android

7) Pasamos los cambios de master a prod_env

`
git checkout prod_env
git merge master
`

8) Si queremos, actualizar el dump de la base de prod ( ver al inicio del doc )


### Build Android

Como tenemos distinto nombre de package en Android e iOS ( ar.org.asdra.finder y org.asdra.finder) respectivamente, usamos dos branches para separar esto.

master -> prod_env ( para iOS) -> prod_env_android ( para Android)

Así que para android hacemos:

```
git checkout prod_env_android

git merge prod_env
```

Validar que el config.xml tiene package ar.org.asdra.finder y que la versión está actualizada.

Hacer el build:

```
cordova build android --release

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore snoop-release.keystore platforms/android/build/outputs/apk/android-release-unsigned.apk AsdraFinder

#passwd: snoop123

$ANDROID_HOME/build-tools/23.0.1/zipalign -v 4 platforms/android/build/outputs/apk/android-release-unsigned.apk AsdraFinder-release-$ASDRA_VERSION.apk
```

me deja por ej:
AsdraFinder-release-4.0.1.apk

7) Play Store
https://play.google.com/apps/publish/?dev_acc=15068384379661873432#MarketListingPlace:p=ar.org.asdra.finder

Listo subí la 4.1.1 a prod y la publiqué


### Build iOS

```
git checkout prod_env

cordova build ios
```

FIXES

1) Validad o agregar al plist:

    <key>NSAppTransportSecurity</key>
    <dict>
      <key>NSAllowsArbitraryLoads</key>
      <true/>
    </dict>
    <key>LSApplicationQueriesSchemes</key>
    <array>
      <string>ar.org.asdra.finder</string>
      <string>tel</string>
    </array>


Esto corrige el error:

App Transport Security has blocked a cleartext HTTP (http://) resource load since it is insecure. Temporary exceptions can be configured via your app's Info.plist file.

2) ir al XCode, build settings

Editar BITCODE y ponerle NO



3) Editar los plist de google Maps

Como estamos usando una versión vieja de google maps tiene un settings que no le gusta al xcode7
GoogleMaps.bundle
sacar CFBundleExecutable del Info.plist
	es sacar esto:
	<key>CFBundleExecutable</key>
	<string>GoogleMaps</string>

CFBUndleSupportedPlatforms sacarle iPhoneSimulator
	Saco estas:
	<key>CFBundleSupportedPlatforms</key>
	<array>
		<string>iPhoneSimulator</string>
	</array>

tambien sacarlo de GMSCoreResourcesBundle/Info.plist

4) Correrla

5) product / archive


Si tira un error de que no encuentra el CDV header.h 
ios cordova/cdvviewcontroller.h file not found


ver
http://forum.ionicframework.com/t/cordova-cdvviewcontroller-h-file-not-found-in-xcode-7-1-beta/32232/5
"$(OBJROOT)/UninstalledProducts/$(PLATFORM_NAME)/include"

6) subirlo con el application uploader












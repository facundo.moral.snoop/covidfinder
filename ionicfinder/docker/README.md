Para crear la imagen:

```
docker build -t ionic-snoop -f Dockerfile.buildenv .
```

Eso crea una imagen "ionic-snoop" en la instalación de docker local, que cuando la corremos levanta "ionic serve"

Para correrlo en un proyecto le tenemos que decir cual es el directorio del proyecto, que está montado como un volumen.

En este caso, vamos al directorio donde tenemos el proyecto ionic y en vez de correr `ionic serve`, lo corremos dentro de un container con:

```
docker run -it \
--rm \
-p 8100:8100 \
-p 35729:35729 \
--privileged \
-v /dev/bus/usb:/dev/bus/usb \
-v $PWD:/var/project_home \
ionic-snoop
```

TO_DO: poder hacer el build de android dentro del Dockerfile. Para armarlo ver:

* https://hub.docker.com/r/mkaag/ionic/
* https://hub.docker.com/r/agileek/ionic-framework/
* https://github.com/gleclaire/docker-ionic-framework
* dockerfile que armamos para jenkins que hace build de Android

#!/usr/bin/env node

// Copia archivos extra a los proyectos generados en platforms.

// Este es para los archivos Android
 
var filestocopy = [

 {
    "build-extras.gradle":"platforms/android/build-extras.gradle"
 }


];
 
var fs = require('fs');
var path = require('path');
var mkdirp = require('./mkdirp/mkdirp.js');

// no need to configure below
var rootdir = process.argv[2];

// Solamente si la plataforma android está lista
if (fs.existsSync(path.join(rootdir, 'platforms', 'android'))) {
    filestocopy.forEach(function(obj) {
        Object.keys(obj).forEach(function(key) {
            var val = obj[key];
            var srcfile = path.join(rootdir, key);
            var destfile = path.join(rootdir, val);
            console.log("copying "+srcfile+" to "+destfile);
            var destdir = path.dirname(destfile);
            if (!fs.existsSync(srcfile)) {
                console.log('****** WARNING ****** ' + srcfile + ' doesn\'t exist');
            }
            if (!fs.existsSync(destdir)) {
                console.log('****** Creating dir ' + destdir);
                mkdirp.sync(destdir);
            }
            if (fs.existsSync(srcfile) && fs.existsSync(destdir)) {
                console.log('Copying ' + srcfile + ' as ' + destfile); // ' into ' + destdir + 
                fs.createReadStream(srcfile).pipe(
                fs.createWriteStream(destfile));
            }
        });
    });

}



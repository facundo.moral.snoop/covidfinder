README.md

# Instalar tools

npm install --global gulp && npm install

# Generar el zip

`gulp`

genera el adminsite.zip

# Subir el zip a AWS S3

Ir a https://console.aws.amazon.com/quickstart-website/website/aws-website-asdra-admin-eic2v y actualizar el zip


- Aca encontramos la lista de buckets en amazon "https://console.aws.amazon.com/s3/home?region=us-east-1#"
- Este es el bucket donde se encuentra el codigo del adminsite "aws-website-asdra-admin-eic2v"
- Este es el comando para listar los buckets por la consola "aws s3 ls s3://aws-website-asdra-admin-eic2v"
- Este es el comando para syncronizar los cambios locales del adminsite al bucket correspondiente "aws s3 sync (mi_carpeta)/asdra/adminsite s3://aws-website-asdra-admin-eic2v"
#ASDRA


##Build

Asegurarse de tener instalado:

* git
* cordova y ionic
* build tools para android
* build tools para ios
* cloudfoundry

###instalar npm y después:

    sudo npm install -g cordova
    sudo npm install -g cordova ionic

###Instalar android studio y setear en el .bash_profile:

````
export JAVA_HOME=`/usr/libexec/java_home -v 1.7`
export ANDROID_HOME="/Users/ramiro/Library/Android/sdk"
export ANDROID_TOOLS="/Users/ramiro/Library/Android/sdk/tools"
export ANDROID_PLATFORM_TOOLS="/Users/ramiro/Library/Android/sdk/platform-tools"
export PATH=$PATH:$ANDROID_HOME:$ANDROID_TOOLS:$ANDROID_PLATFORM_TOOLS
````

###instalar xcode, abrirlo para aceptar la licencia

###https://github.com/cloudfoundry/cli/releases


##Build

Para hacer el build, correr:

````
cd dev/asdra/ionicfinder
buildClean.sh
````

###IMPORTANTE: PLUGINS

Las Keys salen del proyecto Google developers console, están con la cuenta asdra_dev@gmail.com y ramiro@snoopconsulting.com

https://console.developers.google.com/project/nomadic-portal-107822?authuser=1

##Despliegue de Node.js de página administrativa en Bluemix

(Cuenta en Bluemix: usr/pwd = asdra.dev@gmail.com/prueba1234)
Ir hasta directorio ionicfinder/www/admin/finderdev

Init y login:

````
   cf api https://api.ng.bluemix.net
   cf login -u asdra.dev@gmail.com -o asdra.dev@gmail.com -s dev

````
Despliegue:

````
   cf push finderdev
````

###Como leer el código

Ver index.html, donde levanta los JS y define la estructura básica del DOM con el tag
      <body data-ng-app="ionicfinder" data-ng-controller="MainCtrl" animation="slide-left-right-ios7">

Después ver el app.js, que tiene la definición del module "ionicfinder" ( referenciado desde el body )

model.js define el modulo ionicfinder.model que tiene el servicio: finderModelService
	( los nombres no son muy felices acá )

desde el run de apps.js ( que es como el main() ), llama al     finderModelService.loadSites()
Ver la definición del servicio.
Este usa ListService, que está definido en bluemixservices.js

Acá defino una factory en vez de un service como el anterior. No sé si hay una razón o quedó así por como evolucionó. Ver http://tylermcginnis.com/angularjs-factory-vs-service-vs-provider/

Volviendo al apps, cuando arranca en el run también crea el mapa con     createMap();
Esto está en map.js

( ojo hay un archivo gmapsplugin_index.js que creo que no se usa?!??!?, como dice el comentario "//   *************************    ESTO ESTÁ ACÁ PARA HACER COPYPASTE  *****************" )

Volviendo al map.js, define unas globales que probablemente sería más prolijo tenerlas en un service de angular. 











